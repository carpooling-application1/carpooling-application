# Carpooling application
Telerik Academy final project
## Description
The application allows you to discover people who need a ride in the same direction. A car-owner can offer rides to people from the community. Passengers willing to share a ride send requests to carpooling drivers and drivers confirm their offers. There are comments built into the App to stay in touch and figure out details.  
https://trello.com/b/YpAH0pDW/carpooling-application    
##Documentation
https://app.swaggerhub.com/apis/rado88x/CarpoolingDocumentation/0.1?fbclid=IwAR3m6SvoHQRGZTmL7XkKnzBrdeR3CAQsGdpefq1mV_8icdkjeafk0bWQuKs
