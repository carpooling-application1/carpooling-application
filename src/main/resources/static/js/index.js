loadTrips();
loadTopDrivers();


function addTripInfo(htmlElement, tripDTO) {
    htmlElement.append(`
   <div class="card border-dark mb-3" style="width: 32.5%" >
  <div class="card-header"  onclick="showTrip(${tripDTO.id})"><img src="https://image.flaticon.com/icons/svg/1/1647.svg" width="10%" style="margin-left: 10%" alt="Destination">${tripDTO.destination}</div>
  <div class="card-body text-dark">
    <h5 class="card-title"><img src="https://image.flaticon.com/icons/svg/169/169400.svg" width="10%" style="margin-left: 3%" alt="Origin"> ${tripDTO.origin}</h5>
    <h6 class="card-text"><img src="https://image.flaticon.com/icons/svg/1142/1142853.svg" width="10%" style="margin-left: 3%" alt="Departure time">&nbsp;&nbsp;${tripDTO.departureTime}</h6>
    <h6 class="card-text"><img src="https://image.flaticon.com/icons/svg/126/126501.svg" width="10%" style="margin-left: 3%" alt="Message">&nbsp;&nbsp;${tripDTO.message}</h6>
   </div>
</div>`);
}

function addTripInfoForUsers(htmlElement, tripDTO) {
    htmlElement.append(`
   <div class="card border-dark mb-3" style="width: 32.5%" >
  <div class="card-header"  onclick="showTrip(${tripDTO.id})"><img src="https://image.flaticon.com/icons/svg/1/1647.svg" width="10%" style="margin-left: 10%" alt="Destination">${tripDTO.destination}</div>
  <div class="card-body text-dark">
    <h5 class="card-title"><img src="https://image.flaticon.com/icons/svg/169/169400.svg" width="10%" style="margin-left: 3%" alt="Origin"> ${tripDTO.origin}</h5>
    <h6 class="card-text"><img src="https://image.flaticon.com/icons/svg/1142/1142853.svg" width="10%" style="margin-left: 3%" alt="Departure time">&nbsp;&nbsp;${tripDTO.departureTime}</h6>
    <h6 class="card-text"><img src="https://image.flaticon.com/icons/svg/126/126501.svg" width="10%" style="margin-left: 3%" alt="Message">&nbsp;&nbsp;${tripDTO.message}</h6>
 <button type="submit" class="btn btn-outline-secondary" onclick="tripDetails(${tripDTO.id})">Passengers</button>
  <button type="submit" class="btn btn-outline-secondary"  onclick="tripComments(${tripDTO.id})">Comments</button>
   </div>
</div>`);
}

function loadPage(number) {
    let currentPage = $("#current-page").val() * 1;

    if (number === 100 && currentPage !== 0) {
        number = currentPage - 1;
        document.getElementById("current-page").value = number;
    } else if (number === 100 && currentPage === 0) {
        number = 0;
        document.getElementById("current-page").value = number;
    } else if (number === 110 && currentPage !== 4) {
        number = currentPage + 1;
        document.getElementById("current-page").value = number;
    } else if (number === 110 && currentPage === 4) {
        number = 4;
        document.getElementById("current-page").value = number;
    }

    if (number !== 100 || number !== 110) {
        document.getElementById("current-page").value = number;

    }

    let btnContainer = document.getElementById("paginationContainer");
    let btns = btnContainer.getElementsByClassName("page");
    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function () {
            let current = document.getElementsByClassName("active");
            if (current.length >= 0) {
                current[0].className = current[0].className.replace(" active", "");
            }
            this.className += " active";
        });
    }


    let term = $('#searchField').val();
    let limit = $('#limit').val().toLowerCase();
    let status = document.getElementById("statusType");
    let calendar = document.getElementById("calendar");

    let pets = $("#petsCheckBox").is(":checked");
    let luggage = $("#luggageCheckBox").is(":checked");
    let smoking = $("#smokingCheckBox").is(":checked");
    let yourTrips = document.getElementById("yourTrips");
    let availablePlacesSearch = document.getElementById("availablePlacesSearch").value;


    let checkBoxes = document.getElementById("checkBoxes");

    if (limit === "available places") {
        limit = "availablePlaces";
        term = availablePlacesSearch;
    } else if (limit === "earliest departure time") {
        limit = "earliestDepartureTime";
        term = calendar;
    } else if (limit === "latest departure time") {
        limit = "latestDepartureTime";
        term = calendar;
    }

    if (term === null || term === "" || term === undefined) {
        limit = "";
    }

    if (status.style.display === "inline") {
        limit = "status";
        term = status.value.toUpperCase();

        if (status.value === "Status Type") {
            term = "";
        }
    }
    if (calendar.style.display === "inline") {
        term = calendar.value;
    }


    let sortBy = document.getElementById("sortBy").value;
    if (sortBy === "Sort by") {
        sortBy = "";
    } else if (sortBy === "Departure time") {
        sortBy = "departureTime";
    } else if (sortBy === "Destination") {
        sortBy = "destination";
    } else if (sortBy === "Origin") {
        sortBy = "origin";
    }

    let _start = number;
    let _end = 6;

    let url = `http://localhost:8080/api/trips?sortBy=${sortBy}&_start=${_start}&_end=${_end}&${limit}=${term}`;
    if (checkBoxes.style.display === "inline") {
        url = `http://localhost:8080/api/trips/limitations?sortBy=${sortBy}&_start=${_start}&_end=${_end}&pets=${pets}&luggage=${luggage}&smoking=${smoking}`;
    }
    if (yourTrips.style.display === "none") {
        url = `http://localhost:8080/api/users/trips?&sortBy=${sortBy}&_start=${_start}&_end=${_end}`;
    }

    $.ajax({
        url: url,
        method: 'GET',
        success: function (response) {
            $('#trips').html('');
            $.each(response, function (i) {
                if (yourTrips.style.display === "none") {
                    addTripInfoForUsers($('#trips'), response[i]);
                } else {
                    addTripInfo($('#trips'), response[i]);
                }
            })

        }
    });
}


function loadTrips() {
    $.ajax({
        url: `http://localhost:8080/api/trips?_start=0&_end=6`,
        method: 'GET',
        success: function (response) {
            $('#trips').html('');
            $.each(response, function (i) {
                addTripInfo($('#trips'), response[i]);
            })
        }
    })
}


function loadTopDrivers() {
    $.ajax({

        url: `http://localhost:8080/api/users/top-drivers`,
        method: 'GET',
        success: function (response) {

            $('#drivers').html('');
            $.each(response, function (i) {
                let rating = fillInStars(response[i].ratingAsDriver);
                let ratingAsPassenger = fillInStars(response[i].ratingAsPassenger);

                $("#drivers").append(`
 <div class="card border-dark mb-3" style="width: 19.3%">
 <div style="height: 180px; width: 200px">
 <img class="card-img-top" src="/api/users/${response[i].id}/avatar" alt="Card image cap" style="object-fit: contain; width: 250px; height: 180px;">
 </div>
  <div class="card-header">${response[i].username}</div>
  <div class="card-body text-dark">
    <h5 class="card-title">${response[i].firstName} ${response[i].lastName}</h5>
    <h6 class="card-text"> <img src="https://image.flaticon.com/icons/svg/784/784677.svg" width="10%" alt="Email">&nbsp;&nbsp; ${response[i].email}<br>
      <img src="https://image.flaticon.com/icons/svg/149/149384.svg" width="10%" alt="Phone"> &nbsp;&nbsp;${response[i].phone}<br></h6>
    <div id="star"  style="text-align: center" > <img src="https://image.flaticon.com/icons/svg/1464/1464628.svg" width="10%" alt="Rating as driver"> ${rating}</div>
    <div id="star"  style="text-align: center" > <img src="https://image.flaticon.com/icons/svg/1042/1042311.svg" width="10%" alt="Rating as passenger">${ratingAsPassenger}</div>
   </div>
</div>`);
            })
        }
    })
}

function fillInStars(rating) {
    let stars = [];
    rating = Math.round(rating);
    for (let j = 1; j <= 5; j++) {
        if (j <= rating) {
            stars.push(`<span class="fa fa-star"  aria-hidden="true" style="color: gold;"></span>`);
        } else {
            stars.push(`<span class="fa fa-star"></span>`);
        }
    }
    return stars.join(" ");
}


function refreshToken() {
    $.ajax({
        type: 'GET',
        url: `http://localhost:8080/api/users/refresh`
    });
}

$(document).ready(function () {
    if ($.cookie('access_token') == null) {
        showContent("guest");
    } else {
        refreshToken();
        showContent("authorized");
    }
    setInterval(function () {
        if ($.cookie('access_token') != null) {
            refreshToken();
        }
    }, 60000);
})
;

function showContent(className) {
    $('.' + className).show();
}

function hideContent(className) {
    $('.' + className).hide();
}

