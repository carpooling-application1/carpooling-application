function showProfile() {
    $.ajax({
        url: `http://localhost:8080/api/users/`,
        method: 'GET',
        success: function (response) {
            let rating = fillInStars(response.ratingAsDriver);
            let ratingAsPassenger = fillInStars(response.ratingAsPassenger);
            $('#userID').html('').append(`${response.id}`);

            $('#myProfileText').html('')
                .append(`
          <div class="card" style="width: 100%;">
  <img class="card-img-top" src="/api/users/${response.id}/avatar" alt="Avatar photo" style="width: 30%; margin-left: 35%">
  <div class="card-body">
    <h5 class="card-title" style="text-align: center">${response.username}</h5>
    <p class="card-text" style="text-align: left"><div class="row"><div class="column" style="width: 50%;" >
   <img src="https://image.flaticon.com/icons/svg/1232/1232781.svg" width="10%" style="margin-left: 3%" alt="Name">&nbsp;&nbsp;  ${response.firstName} ${response.lastName}<br>
   <img src="https://image.flaticon.com/icons/svg/784/784677.svg" width="10%" style="margin-left: 3%" alt="Email">&nbsp;&nbsp; ${response.email}<br>
   <img src="https://image.flaticon.com/icons/svg/149/149384.svg" width="10%"  style="margin-left: 3%" alt="Phone"> &nbsp;&nbsp;&nbsp; ${response.phone}</div>
   <div class="column" style="width: 50%; text-align: center"> <img src="https://image.flaticon.com/icons/svg/1464/1464628.svg" width="10%" alt="Rating as driver"> ${rating}<br>
   <img src="https://image.flaticon.com/icons/svg/1042/1042311.svg" width="10%" alt="Rating as passenger">  ${ratingAsPassenger}<br></div>
    </p></div></div>
        </div>
        <button type="submit" class="btn btn-outline-secondary" onclick="editUserModal(${response.id})" style="margin-left: 81%; width: 82px;">Edit</button>
     
                 `);
        }
    });


    let modal = document.getElementById("myProfile");
    let span = document.getElementById("myProfileClose");

    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    };

    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
}

$(document).ready(function () {
    $("#register").click(function () {
        let modal = document.getElementById("myModal");
        let span = document.getElementById("myModalClose");
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";

        };
        window.onclick = function (event) {
            if (event.target === modal) {
                modal.style.display = "none";
            }
        };
    });


    $("#registerForm").submit(function (event) {
        event.preventDefault();
        let u = document.getElementById("username").value;
        let f = document.getElementById("firstName").value;
        let l = document.getElementById("lastName").value;
        let p = document.getElementById("password").value;
        let e = document.getElementById("email").value;
        let ph = document.getElementById("phone").value;
        let body = {
            username: u,
            firstName: f,
            lastName: l,
            email: e,
            password: p,
            phone: ph
        };
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/api/users/register',
            contentType: 'application/json',
            data: JSON.stringify(body),

            success: function () {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'You have registered successfully!',
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function () {
                    $("#myModal").hide()
                }, 2500);
                document.getElementById("registerForm").reset();
                loadTrips();
            },
            error: function (request) {
                if (request.status === 409) {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: (JSON.stringify(request.responseJSON.message)),
                        footer: 'Failed to create user!'
                    });

                }
                if (request.status === 400) {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: JSON.stringify(JSON.parse(request.responseText).errors[0].defaultMessage),
                        footer: 'Failed to create user!'
                    });

                }
            }

        });
    });


    $("#logIn").click(function () {
        let modal = document.getElementById("myModal2");
        let span = document.getElementById("myModal2Close");
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
        };
        window.onclick = function (event) {
            if (event.target === modal) {
                modal.style.display = "none";
            }
        }
    });


    $("#logOut").click(function (logout) {
        logout.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/api/users/logout',
            contentType: 'application/json',
            data: JSON.stringify({}),


            success: function () {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'You have successfully logged out!',
                    showConfirmButton: false,
                    timer: 1500
                });
                hideContent("authorized");
                showContent("guest");
                loadTrips();
                document.getElementById("allTrips").style.display = "none";
                document.getElementById("yourTrips").style.display = "inline";

            },

            error: function () {
                alert("Errors sux")
            },

        });
    });

    $("#loginForm").submit(function (event) {
        event.preventDefault();
        let u = $("#usernameLogIn").val();
        let p = $("#passwordLogIn").val();
        let body = {
            username: u,
            password: p
        };

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/api/users/authenticate',
            contentType: 'application/json',
            data: JSON.stringify(body),
            success: function () {
                $("#myModal2").hide();
                $("#loginForm").find("input[type=text], input[type=password]").val("");
                hideContent("guest");
                showContent("authorized");
                loadTrips();
            },
            error: function () {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Wrong username or password! ' +
                        'Try again?',
                    footer: ''
                });
            }
        })
    });


    $("#editUserForm").submit(function (event) {
        event.preventDefault();
        let id = $("#editUserId").val();
        let firstName = $("#firstNameEdit").val();
        let lastName = $("#lastNameEdit").val();
        let phone = $("#phoneEdit").val();
        let email = $("#emailEdit").val();

        let body = {
            id: id,
            firstName: firstName,
            lastName: lastName,
            phone: phone,
            email: email,

        };
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/api/users',
            contentType: 'application/json',
            data: JSON.stringify(body),
            success: function () {
                Swal.fire(
                    'Good job!',
                    'User successfully updated!',
                    'success'
                );

                $("#myModal1").hide();

            },
            error: function (error) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: JSON.stringify(JSON.parse(error.responseText).errors[0].defaultMessage),
                    footer: 'Failed to update user!'
                });

            }
        })
    });

});

$(function () {

    $("form#data").submit(function (event) {
        let id = $('#userID').text();
        event.preventDefault();

        let url = `http://localhost:8080/api/users/${id}/avatar`;
        let image_file = $('#image_file').get(0).files[0];

        let formData = new FormData();
        formData.append("image", image_file);

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function () {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'You have successfully uploaded your new avatar!',
                    showConfirmButton: false,
                    timer: 1500
                });
                showProfile();


            }, error(error) {
                if (error.status===400){
                    Swal.fire({
                        position: 'top',
                        type: 'error',
                        title: 'Oops...',
                        text: "Make sure you have chosen a file to upload!",
                        showConfirmButton: false,
                        timer: 2500
                    });
                }else if(error.status===413){
                    Swal.fire({
                        position: 'top',
                        type: 'error',
                        title: 'Oops...',
                        text: JSON.stringify(error.responseText),
                        showConfirmButton: false,
                        timer: 2500
                    });
                }
            }
        });
        return false;
    });

});


function editUserModal() {
    document.getElementById("myProfile").style.display = "none";
    let modal = document.getElementById("myModal1");
    let span = document.getElementById("myModal1Close");
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    };
    window.onclick = function (even) {
        if (even.target === modal) {
            modal.style.display = "none";
        }
    };


    $.ajax({
        url: `http://localhost:8080/api/users/`,
        method: 'GET',
        success: function (response) {
            document.getElementById("editUserId").value = response.id;
            document.getElementById("firstNameEdit").value = response.firstName;
            document.getElementById("lastNameEdit").value = response.lastName;
            document.getElementById("phoneEdit").value = response.phone;
            document.getElementById("emailEdit").value = response.email;
        }
    });
}