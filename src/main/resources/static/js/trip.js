$(document).ready(function () {

    $("#allTrips").click(function () {

        let filter = document.getElementById("limit");
        let term = document.getElementById("searchField");
        let button = document.getElementById("searchBtn");

        term.style.display = "inline";
        filter.style.display = "inline";
        button.style.display = "inline";

        loadTrips();
        document.getElementById("allTrips").style.display = "none";
        document.getElementById("yourTrips").style.display = "inline";

    });


    $("#yourTrips").click(function () {
        let filter = document.getElementById("limit");
        let term = document.getElementById("searchField");
        let status = document.getElementById("statusType");
        let button = document.getElementById("searchBtn");
        let buttonLmts = document.getElementById("searchBtnLimitations");
        let calendar = document.getElementById("calendar");
        let checkBoxes = document.getElementById("checkBoxes");

        filter.style.display = "none";
        term.style.display = "none";
        status.style.display = "none";
        buttonLmts.style.display = "none";
        button.style.display = "none";
        calendar.style.display = "none";
        checkBoxes.style.display = "none";


        let sortBy = document.getElementById("sortBy").value;

        if (sortBy === "Sort by") {
            sortBy = "";
        } else if (sortBy === "Departure time") {
            sortBy = "departureTime";
        } else if (sortBy === "Available places") {
            sortBy = "availablePlaces";
        } else if (sortBy === "Destination") {
            sortBy = "destination";
        } else if (sortBy === "Origin") {
            sortBy = "origin";
        }

        $.ajax({
            url: `http://localhost:8080/api/users/trips?sortBy=${sortBy}&_start=0&_end=6`,
            method: 'GET',
            success: function (response) {
                document.getElementById("allTrips").style.display = "inline";
                document.getElementById("yourTrips").style.display = "none";
                $('#trips').html('');
                $.each(response, function (i) {
                    addTripInfoForUsers($('#trips'), response[i]);
                })
            }
        })
    });

    $("#addTrip").click(function () {
        let modal = document.getElementById("myModal3");
        let span = document.getElementById("myModal3Close");
        modal.style.display = "block";
        span.onclick = function () {
            modal.style.display = "none";
        };
        window.onclick = function (even) {
            if (even.target === modal) {
                modal.style.display = "none";
            }
        }
    });


    $("#addTripForm").submit(function (event) {
        event.preventDefault();

        let car = $("#carModel").val();
        let msg = $("#message").val();
        let origin = $("#origin").val();
        let destination = $("#destination").val();
        let date = $("#date").val();
        let availablePlaces = $("#availablePlaces").val();
        let l = $("#luggage").is(":checked");
        let p = $("#pets").is(":checked");
        let s = $("#smoking").is(":checked");

        let body = {
            carModel: car,
            message: msg,
            origin: origin,
            destination: destination,
            departureTime: date,
            availablePlaces: availablePlaces,
            smoking: s,
            pets: p,
            luggage: l
        };
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/api/trips',
            contentType: 'application/json',
            data: JSON.stringify(body),
            success: function () {
                Swal.fire(
                    'Good job!',
                    'Trip successfully created!',
                    'success'
                );

                $("#myModal3").hide();
                document.getElementById("addTripForm").reset();
            },
            error: function () {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    // text: JSON.stringify(JSON.parse(request.responseText).errors[0].defaultMessage),
                    //   text: JSON.parse(JSON.stringify(request.responseText)),
                    text: 'Something went wrong! Make sure the fields for origin, destination, available places and departure time (in the future) are filled in :)',
                    footer: 'Failed to create trip!'
                });

            }
        })
    });


    $("#editTripForm").submit(function (event) {
        event.preventDefault();
        let id = $("#editTripId").val();
        let car = $("#carModelEdit").val();
        let msg = $("#messageEdit").val();
        let origin = $("#originEdit").val();
        let destination = $("#destinationEdit").val();
        let date = $("#dateEdit").val();
        let availablePlaces = $("#availablePlacesEdit").val();
        let l = $("#luggageEdit").is(":checked");
        let p = $("#petsEdit").is(":checked");
        let s = $("#smokingEdit").is(":checked");

        let body = {
            id: id,
            carModel: car,
            message: msg,
            origin: origin,
            destination: destination,
            departureTime: date,
            availablePlaces: availablePlaces,
            smoking: s,
            pets: p,
            luggage: l
        };
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/api/trips',
            contentType: 'application/json',
            data: JSON.stringify(body),
            success: function () {
                Swal.fire(
                    'Good job!',
                    'Trip successfully updated!',
                    'success'
                );
                loadTrips();
                showTrip(id);
                $("#myModal4").hide()
            },
            error: function (error) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                  text: 'Something went wrong! Make sure the fields for origin, destination, available places and departure time (in the future) are filled in.',
                    footer: 'Failed to update trip!'
                });

            }
        })
    });


});


function editTripModal(id) {
    let modal = document.getElementById("myModal4");
    let span = document.getElementById("myModal4Close");
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    };
    window.onclick = function (even) {
        if (even.target === modal) {
            modal.style.display = "none";
        }
    };


    $.ajax({
        url: `http://localhost:8080/api/trips/${id}`,
        method: 'GET',
        success: function (response) {
            document.getElementById("editTripId").value = response.id;

            document.getElementById("originEdit").value = response.origin;
            document.getElementById("destinationEdit").value = response.destination;
            document.getElementById("messageEdit").value = response.message;
            document.getElementById("availablePlacesEdit").value = response.availablePlaces;
            document.getElementById("carModelEdit").value = response.carModel;
            document.getElementById("petsEdit").checked = response.arePetsAllowed === true;
            document.getElementById("luggageEdit").checked = response.luggageAllowed === true;
            document.getElementById("smokingEdit").checked = response.smokingAllowed === true;

            let tripDate = response.departureTime;
            tripDate = tripDate.concat(":00");


            document.getElementById("dateEdit").value = tripDate.slice(0, 10) + "T" + tripDate.slice(11);
        }
    });
}


function tripComments(id) {
    $.ajax({
        url: `http://localhost:8080/api/trips/${id}`,
        method: 'GET',
        success: function (response) {
            $('#commentsText').html('');
            $("#placeForBtn").html('').append(`<button type="button" class="btn btn-info pull-right"
            style = "color: white; background-color: #dddddd; border: white"
            onclick = "postComment(${id})">Post</button>`);

            $.each(response.comments, function (i) {

                $("#commentsText").append(`
            <li class="media">
                    <img src=${response.comments[i].author.avatarUri} alt="Avatar photo" class="Avatar photo">
                    <div class="media-body">
                    <span class="text-muted pull-right">
                </span>
                <strong class="text-success">${response.comments[i].author.username}</strong>
                    <p>
                   ${response.comments[i].message}
                    </p>
                    </div>
                    </li>`);
            });
        }
    });

    let modal = document.getElementById("comments");
    let span = document.getElementById("commentsClose");
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    };
    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
}

function postComment(id) {
    let comment = $("#newComment").val();
    let body = {
        message: comment
    };
    $.ajax({
        type: 'POST',
        url: `http://localhost:8080/api/trips/${id}/comments`,
        contentType: 'application/json',
        data: JSON.stringify(body),
        success: function () {
            Swal.fire(
                'Good job!',
                'Your comment was successfully added!',
                'success'
            );
            $("#comments").hide();
            document.getElementById("newComment").value = ('');

        },
        error: function (request) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: JSON.stringify(JSON.parse(request.responseText).message),
                footer: 'Failed to add your comment!'
            });
        }
    });
}

function tripDetails(id) {
    $.ajax({
        url: `http://localhost:8080/api/trips/${id}`,
        method: 'GET',
        success: function (response) {
            $('#tripDetailsText').html('').append(`
<h3 style="text-align: center">Driver</h3>
<div class="card" style="width: 49%;; margin-left: 25%">
<div style="height: 200px;">
  <img class="card-img-top" src=${response.driver.avatarUri} alt="Avatar photo"  style="object-fit: contain; width: 272px; height: 180px;">
  </div>
  <div class="card-top">
    <h5 class="card-title">${response.driver.username}</h5>
    <p class="card-text" style="text-align: left">
    <img src="https://image.flaticon.com/icons/svg/1232/1232781.svg" width="7%" style="margin-left: 3%" alt="Name">&nbsp;&nbsp;${response.driver.firstName} ${response.driver.lastName}<br>
 <img src="https://image.flaticon.com/icons/svg/784/784677.svg" width="7%" style="margin-left: 3%" alt="Email">&nbsp;&nbsp;  ${response.driver.email}<br>
   <img src="https://image.flaticon.com/icons/svg/149/149384.svg" width="7%"  style="margin-left: 3%" alt="Phone"> &nbsp;&nbsp;&nbsp; ${response.driver.phone}<br>
    </div> </div><br><h3 style="text-align: center">Passengers</h3>`);
            $.each(response.passengers, function (i) {
               $("#tripDetailsText").append(`
<div class="card" style="width: 49%;">
<div style="height: 200px;">
  <img class="card-img-top" src=${response.passengers[i].avatarUri} alt="Avatar photo" style="object-fit: contain; width: 272px; height: 180px;">
  </div>
  <div class="card-top">
    <h5 class="card-title">${response.passengers[i].username}</h5>
    <p class="card-text" style="text-align: left">
    <img src="https://image.flaticon.com/icons/svg/1232/1232781.svg" width="7%" style="margin-left: 3%" alt="Name">&nbsp;&nbsp;${response.passengers[i].firstName} ${response.passengers[i].lastName}<br>
    <img src="https://image.flaticon.com/icons/svg/784/784677.svg" width="7%" style="margin-left: 3%" alt="Email">&nbsp;&nbsp;${response.passengers[i].email}<br>
   <img src="https://image.flaticon.com/icons/svg/149/149384.svg" width="7%"  style="margin-left: 3%" alt="Phone">&nbsp;&nbsp;${response.passengers[i].phone}<br>
   <img src="https://image.flaticon.com/icons/svg/942/942796.svg" width="7%"  style="margin-left: 3%" alt="Status">&nbsp;&nbsp;${response.passengers[i].status}<br>    
    <select id="statusOption" class="form-control" onchange="passengerChangeStatus(${response.id}, $(this).val(),${response.passengers[i].userId})">
    <option>-</option>
    <option>Accepted</option>
    <option>Pending</option>
    <option>Rejected</option>
    <option>Absent</option>
    <option>Canceled</option></select> </p>
    </div>
</div>
        </div>`);
            })
        }
    });

    let modal = document.getElementById("tripDetails");
    let span = document.getElementById("tripDetailsClose");
    modal.style.display = "block";
    span.onclick = function () {
        modal.style.display = "none";
    };
    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    };
}

function loadOptions(tripDTO) {

    let s = tripDTO.smokingAllowed;
    if (s === true) {
        s = "Smokers are welcome to this trip!"
    } else {
        s = "Smoking is forbidden!"
    }
    let p = tripDTO.arePetsAllowed;
    if (p === true) {
        p = "Pets are welcome!"
    } else p = "No pets on board!"
    let l = tripDTO.luggageAllowed;
    if (l === true) {
        l = "There is enough space for your luggage!"
    } else l = "Not enough space for luggage!"
    let a = tripDTO.availablePlaces;
    let car = tripDTO.carModel;


    $("#options").html(`
            <h6 style="text-decoration: underline" onclick="$('#options').hide(); $('#showOptions').show()">Hide trip details</h6>
            <h6 > Car model : ${car}</h6>
            <h6 > Available places : ${a} </h6>
            <h6> Smoking : ${s} </h6>
            <h6>Pets : ${p}</h6>
            <h6>Luggage : ${l}</h6>

            `)
}


function showTrip(id) {
    let xhr = $.ajax({
        url: `http://localhost:8080/api/trips/` + id,
        method: 'GET',
        success: function (response) {
            let attribute = xhr.getResponseHeader("status");
            $('#singleTrip').html(`
   <div class="card border-dark mb-3" style="width:98.5%">
  <div class="card-header"  onclick="$('#singleTrip').hide(); $('#tripList').show();">Destination: ${response.destination}</div>
  <div class="card-body text-dark">
    <h5 class="card-title">Origin: ${response.origin}</h5>
    <h6 class="card-text">Departure time: ${response.departureTime}</h6>
    <h6 class="card-text">Message: ${response.message}</h6>
    <h6 id="showOptions" style="text-decoration: underline" onclick="$(this).hide();$('#options').show()">See trip details</h6>
    <div id="options"  style="display: none"></div>
    
    <h6 class="card-text">Status: ${response.status}</h6> </div>
    
      <button id="editTrip" class="btn btn-outline-secondary" onclick="editTripModal(${response.id})" style="display: none; margin-left: 20%">Edit Trip</button>
      <button id="applyForTrip" class="btn btn-outline-secondary" onclick="applyForTrip(${response.id})" style="display: none; margin-left: 20%">Apply</button>
      <button id="leaveTrip" class="btn btn-outline-secondary" onclick="leaveTrip(${response.id})" style="display: none; margin-left: 20%">Leave trip</button>
   
</div>`);
            loadOptions(response);
            let text1 = "<ul>";


            if (attribute === "owner") {
                addChangeStatusElement($('#singleTrip'), response);
                $('#singleTrip').append(`<div align="center" style="border: #dddddd"> You are the owner of this trip!</div>`);

                $('#singleTrip').append(``);

                if (response.status === "Done") {
                    $('#singleTrip').append(`<h4 align="center"> You can rate the passengers of this journey : </h4>`);
                    response.passengers.forEach(function f(item) {
                        let ratingAsPassenger = fillInStars(item.ratingAsPassenger);

                        text1 += `<div class="card border-dark mb-3" style="width: 31%">
 <div style="height: 180px; width: 200px">
 <img class="card-img-top" src="/api/users/${item.userId}/avatar" alt="Card image cap" style="object-fit: contain; width: 280px; height: 180px;">
 </div>
  <div class="card-header">${item.username}</div>
  <div class="card-body text-dark">
    <h5 class="card-title">${item.firstName} ${item.lastName}</h5>
     <img src="https://image.flaticon.com/icons/svg/149/149384.svg" width="10%" alt="Phone"> &nbsp;&nbsp;${item.phone}
 <div id="star" > <img src="https://image.flaticon.com/icons/svg/1042/1042311.svg" width="10%" alt="Rating as passenger">${ratingAsPassenger}</div>
   </h6>
   <div style="text-align: center"><img src="https://image.flaticon.com/icons/svg/942/942826.svg" width="10%" alt="Rate">
   <span class="star-rating star-5">
  <input  type="radio" name="rating" value="1" onclick="ratePassenger(${response.id}, ${item.userId}, 1)"><i></i>
  <input  type="radio" name="rating" value="2" onclick="ratePassenger(${response.id}, ${item.userId}, 2)"><i></i>
  <input  type="radio" name="rating" value="3" onclick="ratePassenger(${response.id}, ${item.userId}, 3)"><i></i>
  <input  type="radio" name="rating" value="4" onclick="ratePassenger(${response.id}, ${item.userId}, 4)"><i></i>
  <input  type="radio" name="rating" value="5" onclick="ratePassenger(${response.id}, ${item.userId}, 5)"><i></i>
</span>
</div>
    </div>
</div>&nbsp;`;
                    });
                    text1 += "</ul>";
                    $("#singleTrip").append(text1);
                }

                if (response.status === "Available") {
                    document.getElementById("editTrip").style.display = "inline";

                }
            }
            if (attribute === "passenger") {
                document.getElementById("leaveTrip").style.display = "inline";

                if (response.status === "Available") {
                    $('#singleTrip').html(`
                   <div class="card border-dark mb-3" style="width:98.5%">
  <div class="card-header"  onclick="$('#singleTrip').hide(); $('#tripList').show();">Destination: ${response.destination}</div>
  <div class="card-body text-dark">
    <h5 class="card-title">Origin: ${response.origin}</h5>
    <h6 class="card-text">Departure time: ${response.departureTime}</h6>
    <h6 class="card-text">Message: ${response.message}</h6>
    <h6 id="showOptions" style="text-decoration: underline" onclick="$(this).hide();$('#options').show()">See trip details</h6>
    <div id="options"  style="display: none"></div>
    <h6 class="card-text">Status: ${response.status}</h6> </div>
      <button id="applyForTrip" class="btn btn-outline-secondary" onclick="applyForTrip(${response.id})" style="display: none; margin-left: 20%">Apply</button>
      <button id="leaveTrip" class="btn btn-outline-secondary" onclick="leaveTrip(${response.id})" style="display: inline; margin-left: 20%">Leave trip</button>
   </div>   
   
                `);
                    loadOptions(response);

                }

                if (response.status === "Done") {
                    let rating = fillInStars(response.driver.ratingAsDriver);

                    $('#singleTrip').append(`  
  <h4 align="center"> You can rate the driver of this journey : </h4>
  
  
  <div class="card border-dark mb-3" style="width: 31%; margin-left: 35%">
 <div style="height: 180px; width: 200px">
 <img class="card-img-top" src="/api/users/${response.driver.id}/avatar" alt="Card image cap" style="object-fit: contain; width: 290px; height: 180px;">
 </div>
  <div class="card-header">${response.driver.username}</div>
  <div class="card-body text-dark">
    <h5 class="card-title">${response.driver.firstName} ${response.driver.lastName}</h5>
     <img src="https://image.flaticon.com/icons/svg/149/149384.svg" width="10%" alt="Phone"> &nbsp;&nbsp;${response.driver.phone}
 <div id="star"  style="text-align: center" > <img src="https://image.flaticon.com/icons/svg/1464/1464628.svg" width="10%" alt="Rating as driver"> ${rating}</div>
  </h6> <div style="text-align: center"><img src="https://image.flaticon.com/icons/svg/942/942826.svg" width="10%" alt="Rate">
  <span class="star-rating star-5">
  <input  type="radio" name="rating" value="1" onclick="rateDriver(${response.id},1)"><i></i>
  <input  type="radio" name="rating" value="2" onclick="rateDriver(${response.id},2)"><i></i>
  <input  type="radio" name="rating" value="3" onclick="rateDriver(${response.id},3)"><i></i>
  <input  type="radio" name="rating" value="4" onclick="rateDriver(${response.id},4)"><i></i>
  <input  type="radio" name="rating" value="5" onclick="rateDriver(${response.id},5)"><i></i>
</span></div>
</div>
    </div>
</div>
 `);
                } else {
                    $('#singleTrip').append(`<div>You have joined this trip!</div>`);

                }


            }
            if (attribute === "observer") {
                if (response.status === "Available") {
                    document.getElementById("applyForTrip").style.display = "inline";
                    $('#singleTrip').append(`<div>You can join this trip!</div>`);
                }
            }
            $("#tripList").hide();
            $("#singleTrip").show();
        }
        ,
        error: function () {
            alert("Error!");
        }
    });
}

function addChangeStatusElement(htmlElement, tripDTO) {
    if (tripDTO.status === "Done") {
        return;
    }
    let text = ``;
    let arr;
    if (tripDTO.status === "Available") {
        let arrA = ["Available", "Booked", "Canceled"];
        text = `<select class="form-control" onchange="tripChangeStatus(${tripDTO.id}, $(this).val())">`;

        text += `<option selected="selected">Available</option>`;
        text += `<option>Booked</option>`;
        text += `<option>Canceled</option>`;
        text += `</select>`;
    }
    if (tripDTO.status === "Booked") {
        text = `<select class="form-control" onchange="tripChangeStatus(${tripDTO.id}, $(this).val())">`;

        text += `<option selected="selected">Booked</option>`;
        text += `<option>Ongoing</option>`;
        text += `<option>Canceled</option>`;
        text += `</select>`
    }
    if (tripDTO.status === "Ongoing") {
        text += `<button type="submit" class="btn btn-outline-secondary" onclick="tripChangeStatus(${tripDTO.id}, 'Done'); showTrip(${tripDTO.id})"">Finish trip</button>`
    }
    htmlElement.append(text);
}

function ratePassenger(tripid, passengerId, rating) {
    let body = rating;
    $.ajax({
        url: `http://localhost:8080/api/trips/${tripid}/passengers/${passengerId}/rate`,
        method: "POST",
        contentType: 'application/json',
        data: JSON.stringify(body),

        success: function () {
            Swal.fire(
                'Good job!',
                'Thank you for your feedback!!',
                'success'
            );
            loadTopDrivers();
            showTrip(tripid);
        },
        error(error) {
            Swal.fire({
                position: 'top',
                type: 'error',
                title: 'Oops...',
                text: (JSON.stringify(error.responseJSON.message)),
                showConfirmButton: false,
                timer: 2500
            });

        }
    });
}


function rateDriver(tripId, rating) {
    let body = rating;
    $.ajax({
        url: `http://localhost:8080/api/trips/${tripId}/driver/rate`,
        method: "POST",
        contentType: 'application/json',
        data: JSON.stringify(body),

        success: function () {
            Swal.fire(
                'Good job!',
                'Thank you for your feedback!!',
                'success'
            );
            loadTopDrivers();
            showTrip(tripId);
        },
        error(error) {
            Swal.fire({
                position: 'top',
                type: 'error',
                title: 'Oops...',
                text: (JSON.stringify(error.responseJSON.message)),
                showConfirmButton: false,
                timer: 2500
            });

        }
    });
}


function tripChangeStatus(id, status) {
    $.ajax({
        url: `http://localhost:8080/api/trips/${id}?status=${status}`,
        method: 'PATCH',

        success: function () {
            showTrip(id);
        },
        error: function () {
        }
    });
}

function passengerChangeStatus(id, status, passengerId) {
    if (status !== "-") {
        $.ajax({
            url: `http://localhost:8080/api/trips/${id}/passengers/${passengerId}?status=${status}`,
            method: 'PATCH',
            success: function () {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'You have successfully changed the status of the passenger!',
                    showConfirmButton: false,
                    timer: 1500
                });
                tripDetails(id);
            },
            error: function (error) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: JSON.stringify(JSON.parse(error.responseText).message),
                    footer: 'Failed to change status!'
                });
            }
        });
    }
}

function applyForTrip(id) {
    $.ajax({
        url: `http://localhost:8080/api/trips/${id}/passengers/`,
        method: 'POST',
        success: function () {
            Swal.fire({
                position: 'top',
                type: 'success',
                title: 'You have successfully applied for the trip!',
                showConfirmButton: false,
                timer: 1500
            });
            showTrip(id);
            loadTrips();
        },
        error: function (error) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: JSON.stringify(JSON.parse(error.responseText).message),
                footer: 'Failed to apply for the trip!'
            });
        }
    });
}

function leaveTrip(id) {
    $.ajax({
        url: `http://localhost:8080/api/trips/${id}/passengers/`,
        method: 'DELETE',
        success: function () {
            Swal.fire({
                position: 'top',
                type: 'success',
                title: 'You left the trip!',
                showConfirmButton: false,
                timer: 1500
            });
            showTrip(id);
        },
        error: function (error) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: JSON.stringify(JSON.parse(error.responseText).message),
                footer: 'Failed to left the trip!'
            });
        }
    });
}
