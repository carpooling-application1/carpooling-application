function displayRightSearchTool() {
    let filter = $('#limit').val().toLowerCase();
    let term = document.getElementById("searchField");
    let status = document.getElementById("statusType");
    let button = document.getElementById("searchBtn");
    let buttonLmts = document.getElementById("searchBtnLimitations");
    let calendar = document.getElementById("calendar");
    let checkBoxes = document.getElementById("checkBoxes");
    let availablePlacesSearch = document.getElementById("availablePlacesSearch");


    if (filter === "status") {
        term.style.display = "none";
        status.style.display = "inline";
        button.style.display = "none";
        calendar.style.display = "none";
        checkBoxes.style.display = "none";
        buttonLmts.style.display = "none";
        availablePlacesSearch.style.display = "none";

    } else if (filter === "earliest departure time" || filter === "latest departure time") {
        term.style.display = "none";
        calendar.style.display = "inline";
        button.style.display = "inline";
        status.style.display = "none";
        checkBoxes.style.display = "none";
        buttonLmts.style.display = "none";
        availablePlacesSearch.style.display = "none";

    } else if (filter === "limitations") {
        term.style.display = "none";
        calendar.style.display = "none";
        button.style.display = "none";
        status.style.display = "none";
        checkBoxes.style.display = "inline";
        buttonLmts.style.display = "inline";
        availablePlacesSearch.style.display = "none";
    } else if (filter === "available places") {
        term.style.display = "none";
        calendar.style.display = "none";
        button.style.display = "inline";
        status.style.display = "none";
        checkBoxes.style.display = "none";
        buttonLmts.style.display = "none";
        availablePlacesSearch.style.display = "inline";
    } else {
        term.style.display = "inline";
        button.style.display = "inline";
        status.style.display = "none";
        calendar.style.display = "none";
        checkBoxes.style.display = "none";
        buttonLmts.style.display = "none";

    }
}

function searchByStatus() {
    let term = document.getElementById("statusType").value.toUpperCase();
    let sortBy = document.getElementById("sortBy").value;

    if (sortBy === "Sort by") {
        sortBy = "";
    } else if (sortBy === "Departure time") {
        sortBy = "departureTime";
    } else if (sortBy === "Available places") {
        sortBy = "availablePlaces";
    }
    if (term === "STATUS TYPE") {
        sortBy = "";
    }

    $.ajax({
        url: `http://localhost:8080/api/trips?sortBy=${sortBy}&_start=0&_end=6&status=${term}`,
        method: 'GET',
        success: function (response) {
            $('#trips').html('');
            $.each(response, function (i) {
                addTripInfo($('#trips'), response[i]);
            });
        }
    });
}


$(document).ready(function () {
    $("#searchBtn").click(function () {
        let term = $('#searchField').val();
        let limit = $('#limit').val().toLowerCase();
        let sortBy = document.getElementById("sortBy").value;
        let calendar = document.getElementById("calendar");
        let availablePlacesSearch = document.getElementById("availablePlacesSearch").value;

        if (limit === "available places") {
            limit = "availablePlaces";
            term = availablePlacesSearch;
        } else if (limit === "earliest departure time") {
            limit = "earliestDepartureTime";
            term = calendar.value;
        } else if (limit === "latest departure time") {
            limit = "latestDepartureTime";
            term = calendar.value;
        }

        if (sortBy === "Sort by") {
            sortBy = "";
        } else if (sortBy === "Departure time") {
            sortBy = "departureTime";
        } else if (sortBy === "Available places") {
            sortBy = "availablePlaces";
        } else if (sortBy === "Destination") {
            sortBy = "destination";
        } else if (sortBy === "Origin") {
            sortBy = "origin";
        }
        if (term === undefined || term === null) {
            limit = "";
        }

        $.ajax({
            url: `http://localhost:8080/api/trips?sortBy=${sortBy}&_start=0&_end=6&${limit}=${term}`,
            method: 'GET',
            success: function (response) {
                $('#trips').html('');
                $.each(response, function (i) {
                    addTripInfo($('#trips'), response[i]);
                });
            }
        });
    });

    $("#searchBtnLimitations").click(function () {
        let pets = $("#petsCheckBox").is(":checked");
        let luggage = $("#luggageCheckBox").is(":checked");
        let smoking = $("#smokingCheckBox").is(":checked");
        let sortBy = document.getElementById("sortBy").value;


        if (sortBy === "Sort by") {
            sortBy = "";
        } else if (sortBy === "Departure time") {
            sortBy = "departureTime";
        } else if (sortBy === "Available places") {
            sortBy = "availablePlaces";
        } else if (sortBy === "Destination") {
            sortBy = "destination";
        } else if (sortBy === "Origin") {
            sortBy = "origin";
        }

        $.ajax({
            url: `http://localhost:8080/api/trips/limitations?sortBy=${sortBy}&_start=0&_end=6&pets=${pets}&luggage=${luggage}&smoking=${smoking}`,
            method: 'GET',
            success: function (response) {
                $('#trips').html('');
                $.each(response, function (i) {
                    addTripInfo($('#trips'), response[i]);
                });
            }
        });
    });

});
