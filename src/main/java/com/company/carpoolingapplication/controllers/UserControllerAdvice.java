package com.company.carpoolingapplication.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserControllerAdvice {
    @ExceptionHandler({IllegalStateException.class})
    public ResponseEntity<Object> handleFileSizeLimitExceededException(IllegalStateException e) {
        return new ResponseEntity<>("Please upload a smaller photo! The maximum allowed size is 1MB!", HttpStatus.PAYLOAD_TOO_LARGE);
    }
}


