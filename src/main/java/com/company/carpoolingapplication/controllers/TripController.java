package com.company.carpoolingapplication.controllers;

import com.company.carpoolingapplication.models.PassengerDetails;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.common.TripStatus;
import com.company.carpoolingapplication.modelsDTO.*;
import com.company.carpoolingapplication.security.JwtTokenProvider;
import com.company.carpoolingapplication.services.contracts.CommentService;
import com.company.carpoolingapplication.modelsDTO.CreateTripDTO;
import com.company.carpoolingapplication.modelsDTO.EditTripDTO;
import com.company.carpoolingapplication.modelsDTO.TripDTO;
import com.company.carpoolingapplication.services.contracts.TripService;
import com.company.carpoolingapplication.services.contracts.UserService;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/trips")
public class TripController {
    private TripService service;
    private CommentService commentService;
    private UserService userService;
    private JwtTokenProvider jwtTokenProvider;


    @Autowired
    public TripController(TripService service, CommentService commentService, UserService userService, JwtTokenProvider jwtTokenProvider) {
        this.service = service;
        this.commentService = commentService;
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @GetMapping
    public List<TripDTO> getTrips(@RequestParam(required = false) String sortBy,
                                  @RequestParam int _start,
                                  @RequestParam int _end,
                                  @RequestParam(required = false) String driver,
                                  @RequestParam(required = false) String origin,
                                  @RequestParam(required = false) String destination,
                                  @RequestParam(required = false) Integer availablePlaces,
                                  @RequestParam(required = false) TripStatus status,
                                  @RequestParam(required = false) String earliestDepartureTime,
                                  @RequestParam(required = false) String latestDepartureTime
    ) {
        if (sortBy == null) {
            sortBy = "";
        }
        if (driver != null) {
            try {
                return ModelsMapper.fromTripListToTripDTOList(service.findByDriver(driver, _start, _end, sortBy));
            } catch (IllegalArgumentException ex) {
                return new ArrayList<>();
            }
        }
        if (origin != null) {
            return ModelsMapper.fromTripListToTripDTOList(service.findByOrigin(origin, _start, _end, sortBy));
        }
        if (destination != null) {
            return ModelsMapper.fromTripListToTripDTOList(service.findByDestination(destination, _start, _end, sortBy));
        }
        if (availablePlaces != null) {
            return ModelsMapper.fromTripListToTripDTOList(service.findByAvailablePlaces(availablePlaces, _start, _end, sortBy));
        }
        if (status != null) {
            return ModelsMapper.fromTripListToTripDTOList(service.findByStatus(status, _start, _end, sortBy));
        }
        if (earliestDepartureTime != null) {
            return ModelsMapper.fromTripListToTripDTOList(service.findByEarliestDepartureTime(earliestDepartureTime, _start, _end, sortBy));
        }
        if (latestDepartureTime != null) {
            return ModelsMapper.fromTripListToTripDTOList(service.findByLatestDepartureTime(latestDepartureTime, _start, _end, sortBy));
        }

        return ModelsMapper.fromTripListToTripDTOList(service.findAll(_start, _end, sortBy));
    }

    @GetMapping("/limitations")
    public List<TripDTO> getTripsByLimitations(@RequestParam String sortBy,
                                               @RequestParam int _start,
                                               @RequestParam int _end,
                                               @RequestParam String pets,
                                               @RequestParam String smoking,
                                               @RequestParam String luggage) {
        if (sortBy == null) {
            sortBy = "";
        }
        return ModelsMapper.fromTripListToTripDTOList(service.findByLimitations(pets, smoking, luggage, _start, _end, sortBy));

    }

    @PostMapping
    public void createTrip(@Valid @RequestBody CreateTripDTO createTripDTO, HttpServletRequest httpServletRequest) {
        try {
            Trip trip = ModelsMapper.fromCreateTripDTOtoTrip(createTripDTO);
            User user = getLoggedUser(httpServletRequest);
            service.createTrip(trip, user);
        } catch (
                ConstraintViolationException | IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @GetMapping("/{id}")
    public TripDTO getTrip(@PathVariable long id, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String status = tripOwnership(id, httpServletRequest);
        httpServletResponse.setHeader("status", status);
        return ModelsMapper.fromTripToTripDTO(service.findById(id));
    }

    @PatchMapping("/{id}")
    public void changeTripStatus(@PathVariable long id, @RequestParam String status, HttpServletRequest httpServletRequest) {
        try {
            service.changeTripStatus(id, status, getLoggedUser(httpServletRequest));
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/{id}/passengers")
    public void apply(@PathVariable long id, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            service.apply(id, user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}/passengers")
    public void leaveTrip(@PathVariable long id, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            service.leaveTrip(id, user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @PatchMapping("/{tripId}/passengers/{passengerId}")
    public void changePassengerStatus(@PathVariable long tripId, @PathVariable long passengerId,
                                      @RequestParam String status, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            service.changePassengerStatus(tripId, passengerId, status, user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @PostMapping("/{tripId}/passengers/{passengerId}/rate")
    public void ratePassenger(@PathVariable long tripId, @PathVariable long passengerId,
                              @RequestBody double rating, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            service.ratePassenger(tripId, passengerId, rating, user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }


    @PostMapping("/{id}/driver/rate")
    public void rateDriver(@PathVariable long id, @RequestBody double rating, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            service.rateDriver(id, rating, user);
        } catch (MalformedJwtException | IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @PutMapping
    public void editTrip(@Valid @RequestBody EditTripDTO editTripDTO, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            Trip trip = ModelsMapper.fromEditTripDTOtoTrip(editTripDTO);
            service.editTrip(trip, user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @PostMapping("/{id}/comments")
    public void addComment(@PathVariable long id, @RequestBody CreateCommentDTO createCommentDTO, HttpServletRequest httpServletRequest) {
        try {
            User user = getLoggedUser(httpServletRequest);
            commentService.addComment(id, ModelsMapper.fromCreateCommentDTOtoComment(createCommentDTO), user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    public User getLoggedUser(HttpServletRequest httpServletRequest) {
        String cookie = httpServletRequest.getHeader("Cookie");
        if (cookie == null) {
            throw new IllegalArgumentException("No logged user!");
        }
        int start = cookie.indexOf("access_token=");
        if (start == -1) {
            throw new IllegalArgumentException("No logged user!");
        }
        int end = cookie.indexOf(";", start + 1);
        if (end == -1)
            end = cookie.length();
        String token = cookie.substring(start + "access_token=".length(), end);
        return userService.getByName(jwtTokenProvider.getUsername(token));
    }

    public String tripOwnership(long id, HttpServletRequest httpServletRequest) {
        String status = "observer";
        try {
            User user = getLoggedUser(httpServletRequest);
            long loggedUserID = user.getId();
            Trip trip = service.findById(id);
            if (loggedUserID == trip.getDriver().getId())
                status = "owner";
            else {
                for (PassengerDetails passenger : trip.getPassengerDetails()) {
                    if (passenger.getUser().getId() == loggedUserID) {
                        status = "passenger";
                        break;
                    }
                }
            }
        } catch (IllegalArgumentException e) {
        }
        return status;
    }
}
