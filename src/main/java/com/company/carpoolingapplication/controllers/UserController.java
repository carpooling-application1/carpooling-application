package com.company.carpoolingapplication.controllers;


import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.modelsDTO.*;
import com.company.carpoolingapplication.security.JwtTokenProvider;
import com.company.carpoolingapplication.services.contracts.UserService;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private UserService service;
    private JwtTokenProvider jwtTokenProvider;


    @Autowired
    public UserController(UserService service, JwtTokenProvider jwtTokenProvider) {
        this.service = service;
        this.jwtTokenProvider = jwtTokenProvider;
    }


    @GetMapping("/{username}")
    public UserDTO getUser(@PathVariable String username) {
        try {
            return ModelsMapper.fromUserToUserDTO(service.getByName(username));
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping
    public UserDTO getMyProfile(HttpServletRequest httpServletRequest) {
        try {
            return ModelsMapper.fromUserToUserDTO(getLoggedUser(httpServletRequest));
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        }
    }

    @PostMapping("/register")
    public UserDTO createUser(@Valid @RequestBody CreateUserDTO user) {
        try {
            return service.createUser(ModelsMapper.fromCreateUserDTOtoUser(user));
        } catch (ConstraintViolationException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @PostMapping("/authenticate")
    public JWTToken authorize(@Valid @RequestBody LoginDTO loginDTO, HttpServletResponse response, HttpServletRequest request) {
        return service.authenticate(loginDTO, response);
    }

    @PostMapping("/{id}/avatar")
    public void uploadFile(@PathVariable long id, @RequestParam("image") MultipartFile image, HttpServletRequest request) throws Exception {
        try {
            service.saveImage(image, id, getLoggedUser(request));
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


    @GetMapping("/{id}/avatar")
    public ResponseEntity<Resource> getImage(@PathVariable long id) {
        try {
            File file = service.getImage(id);
            Path path = Paths.get(file.getAbsolutePath());
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            return ResponseEntity.ok().contentLength(file.length()).contentType(MediaType.IMAGE_PNG).body(resource);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/trips")
    public List<TripDTO> getTrips(HttpServletRequest httpServletRequest, @RequestParam(required = false) String sortBy,
                                  @RequestParam int _start,
                                  @RequestParam int _end) {
        if (sortBy == null) {
            sortBy = "";
        }
        try {
            return ModelsMapper.fromTripListToTripDTOList(service.getTrips(getLoggedUser(httpServletRequest), _start, _end, sortBy));
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


    @GetMapping("/top-drivers")
    public List<UserDTO> getTopTenDrivers() {
        return service.getTopTenDrivers();
    }

    @PutMapping
    public UserDTO editUser(@Valid @RequestBody UserDTO dto, HttpServletRequest httpServletRequest) {
        try {
            return service.editUser(dto, getLoggedUser(httpServletRequest));
        } catch (MalformedJwtException ex) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ex.getMessage());
        } catch (IllegalArgumentException | ConstraintViolationException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @PostMapping("/logout")
    public void logout(HttpServletResponse response) {
        service.logout(response);
    }

    @GetMapping("/refresh")
    public void refresh(HttpServletRequest req, HttpServletResponse response) {
        String newToken = service.refresh(getLoggedUser(req).getUsername());
        Cookie cookie = new Cookie("access_token", newToken);
        cookie.setPath("/");
        cookie.setDomain("localhost");
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
    }


    public User getLoggedUser(HttpServletRequest httpServletRequest) {
        String cookie = httpServletRequest.getHeader("Cookie");
        if (cookie == null) {
            throw new IllegalArgumentException("No logged user!");
        }
        int start = cookie.indexOf("access_token=");
        if (start == -1) {
            throw new IllegalArgumentException("No logged user!");
        }
        int end = cookie.indexOf(";", start + 1);
        if (end == -1)
            end = cookie.length();
        String token = cookie.substring(start + "access_token=".length(), end);
        return service.getByName(jwtTokenProvider.getUsername(token));
    }

}
