package com.company.carpoolingapplication.services;


import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.models.common.Role;
import com.company.carpoolingapplication.modelsDTO.*;
import com.company.carpoolingapplication.repositories.PassengerDetailsRepository;
import com.company.carpoolingapplication.repositories.TripRepository;
import com.company.carpoolingapplication.repositories.UserRepository;
import com.company.carpoolingapplication.security.JwtTokenProvider;
import com.company.carpoolingapplication.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository repository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider jwtTokenProvider;
    private AuthenticationManager authenticationManager;
    private PassengerDetailsRepository passengerDetailsRepository;
    private TripRepository tripRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository, PasswordEncoder passwordEncoder,
                           JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager,
                           PassengerDetailsRepository passengerDetailsRepository,
                           TripRepository tripRepository) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.passengerDetailsRepository = passengerDetailsRepository;
        this.tripRepository = tripRepository;
    }

    @Override
    public User getByName(String username) {
        if (repository.findByUsername(username) == null) {
            throw new IllegalArgumentException(String.format("User with username %s does not exist!", username));
        } else {
            User user = repository.findByUsername(username);
            setAverageRating(user);
            return user;
        }
    }

    @Override
    public UserDTO createUser(User user) {
        if (repository.findByUsername(user.getUsername()) != null) {
            throw new IllegalArgumentException(String.format("User with name %s already exists!", user.getUsername()));
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.addRole(Role.ROLE_CLIENT);
        repository.save(user);
        return ModelsMapper.fromUserToUserDTO(user);
    }

    @Override
    public void saveImage(MultipartFile image, long id, User user) throws Exception {
        String folder = Paths.get("").toAbsolutePath().toString() + "/src/main/resources/static/img/";
        byte[] bytes = image.getBytes();
        Path path = Paths.get(folder + image.getOriginalFilename());
        Files.write(path, bytes);
        if (!repository.findById(id).isPresent()) {
            throw new IllegalArgumentException(String.format("User with ID %d does not exist!", id));
        }

        User userById = repository.findById(id).get();
        if (user.getId() != userById.getId()) {
            throw new IllegalArgumentException("You can change only your avatar photo!");
        }
        user.setAvatarUri("../img/" + image.getOriginalFilename());
        repository.save(user);
    }

    @Override
    public File getImage(long id) {
        String avatarUri;
        if (!repository.findById(id).isPresent()) {
            throw new IllegalArgumentException(String.format("User with ID %d does not exist!", id));
        }
        avatarUri = repository.findById(id).get().getAvatarUri().substring(2);
        String path = Paths.get("").toAbsolutePath().toString() + "/src/main/resources/static" + avatarUri;
        File file = new File(path);
        return file;
    }


    @Override
    public JWTToken authenticate(LoginDTO loginDTO, HttpServletResponse response) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));
        String token = jwtTokenProvider.createToken(loginDTO.getUsername(), repository.findByUsername(loginDTO.getUsername()).getRoles());

        Cookie cookie = new Cookie("access_token", token);
        cookie.setPath("/");
        cookie.setDomain("localhost");
        cookie.setMaxAge(3600);
        response.addCookie(cookie);
        return ModelsMapper.fromTokenToTokenDTO(token);
    }

    @Override
    public List<Trip> getTrips(User user, int _start, int _end, String sortBy) {
        List<Trip> tripsAsPassenger = passengerDetailsRepository.getTripsOfUser(user);
        List<Trip> trips = tripRepository.findByDriver(user);
        trips.addAll(tripsAsPassenger);
        if (sortBy.equals("destination")) {
            trips = trips.stream().sorted(Comparator.comparing(Trip::getDestination)).collect(Collectors.toList());
        }
        if (sortBy.equals("departureTime")) {
            trips = trips.stream().sorted(Comparator.comparing(Trip::getDepartureTime)).collect(Collectors.toList());
        }
        if (sortBy.equals("origin")) {
            trips = trips.stream().sorted(Comparator.comparing(Trip::getOrigin)).collect(Collectors.toList());
        }
        List<Trip> result = new ArrayList<>();
        for (int i = _start * _end; i < _start * _end + _end; i++) {
            if (i >= trips.size()) {
                break;
            }
            result.add(trips.get(i));
        }
        return result;
    }

    @Override
    public UserDTO editUser(UserDTO dto, User user) {
        User u;
        if (!repository.findById(dto.getId()).isPresent()) {
            throw new IllegalArgumentException(String.format("User with id %d does not exist!", dto.getId()));
        } else {
            u = repository.getOne(dto.getId());
        }
        if (repository.findByUsername(dto.getUsername()) != null && (!dto.getUsername().equals(user.getUsername()))) {
            throw new IllegalArgumentException(String.format("User with name %s already exists!", dto.getUsername()));
        }
        if (dto.getId() != user.getId()) {
            throw new IllegalArgumentException("You can't change other users information!");
        }
        if (dto.getUsername() != null) {
            u.setUsername(dto.getUsername());
        }
        if (dto.getFirstName() != null) {
            u.setFirstName(dto.getFirstName());
        }
        if (dto.getLastName() != null) {
            u.setLastName(dto.getLastName());
        }
        if (dto.getEmail() != null) {
            u.setEmail(dto.getEmail());
        }
        if (dto.getPhone() != null) {
            u.setPhone(dto.getPhone());
        }
        if (dto.getAvatarUri() != null) {
            u.setAvatarUri(dto.getAvatarUri());
        }
        setAverageRating(u);
        repository.save(u);
        return ModelsMapper.fromUserToUserDTO(u);
    }

    public List<UserDTO> getTopTenDrivers() {
        for (int i = 0; i < repository.findAll().size(); i++) {
            User user = repository.findAll().get(i);
            if (passengerDetailsRepository.getRatingAsDriver(user) == null) {
                continue;
            }
            double rating = passengerDetailsRepository.getRatingAsDriver(user);
            user.setAverageRatingAsDriver(rating);
        }

        for (int i = 0; i < repository.findAll().size(); i++) {
            User user = repository.findAll().get(i);
            if (passengerDetailsRepository.getRatingAsPassenger(user) == null) {
                continue;
            }
            double rating = passengerDetailsRepository.getRatingAsPassenger(user);
            user.setAverageRatingAsPassenger(rating);
        }
        List<User> drivers = repository.findAll().stream()
                .sorted(Comparator.comparing(User::getAverageRatingAsDriver)
                        .reversed()).limit(10).collect(Collectors.toList());
        return ModelsMapper.fromUserListToUserDTOList(drivers);
    }

    private void setAverageRating(User user) {
        if (passengerDetailsRepository.getRatingAsPassenger(user) != null) {
            user.setAverageRatingAsPassenger(passengerDetailsRepository.getRatingAsPassenger(user));
        }
        if (passengerDetailsRepository.getRatingAsDriver(user) != null) {
            user.setAverageRatingAsDriver(passengerDetailsRepository.getRatingAsDriver(user));
        }
    }

    @Override
    public void logout(HttpServletResponse response) {
        Cookie cookie = new Cookie("access_token", "deadCookie");
        cookie.setPath("/");
        cookie.setDomain("localhost");
        cookie.setMaxAge(0);
        response.addCookie(cookie);
    }

    @Override
    public String refresh(String username) {
        return jwtTokenProvider.createToken(username, repository.findByUsername(username).getRoles());
    }
}




