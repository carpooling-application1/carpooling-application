package com.company.carpoolingapplication.services;

import com.company.carpoolingapplication.models.Comment;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.repositories.CommentRepository;
import com.company.carpoolingapplication.repositories.TripRepository;
import com.company.carpoolingapplication.services.contracts.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    private CommentRepository repository;
    private TripRepository tripRepository;

    @Autowired
    public CommentServiceImpl(CommentRepository repository, TripRepository tripRepository) {
        this.repository = repository;
        this.tripRepository = tripRepository;
    }

    @Override
    public void addComment(long tripID, Comment comment, User user) {
        if (tripRepository.findById(tripID) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripID));
        }
        if ((tripRepository.findById(tripID).getPassengerDetails()
                .stream().noneMatch(passengerDetails ->
                        passengerDetails.getUser().getUsername().equals(user.getUsername()))) &&
                (!tripRepository.findById(tripID).getDriver().getUsername().equals(user.getUsername()))) {
            throw new IllegalArgumentException("You can only post a comment for trips that you participate!");
        }
        comment.setAuthor(user);
        comment.setTrip(tripRepository.findById(tripID));
        repository.save(comment);
    }
}
