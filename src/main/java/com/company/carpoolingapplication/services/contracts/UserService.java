package com.company.carpoolingapplication.services.contracts;

import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.modelsDTO.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;

public interface UserService {
    User getByName(String username);

    UserDTO createUser(User user);

    void saveImage(MultipartFile image, long id, User user) throws Exception;

    File getImage(long id);

    JWTToken authenticate(LoginDTO loginDTO, HttpServletResponse response);

    List<Trip> getTrips(User user, int _start, int _end, String sortBy);

    UserDTO editUser(UserDTO dto, User user);

    List<UserDTO> getTopTenDrivers();

    void logout(HttpServletResponse response);

    String refresh(String username);
}
