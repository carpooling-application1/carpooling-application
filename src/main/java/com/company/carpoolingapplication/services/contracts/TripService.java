package com.company.carpoolingapplication.services.contracts;


import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.models.common.TripStatus;

import java.util.List;

public interface TripService {
    void createTrip(Trip trip, User user);

    List<Trip> findAll(int _start, int _end, String sortBy);

    List<Trip> findByDriver(String username, int _start, int _end, String sortBy);

    List<Trip> findByDriver(String username);

    List<Trip> findByStatus(TripStatus status, int _start, int _end, String sortBy);

    List<Trip> findByOrigin(String origin, int _start, int _end, String sortBy);

    List<Trip> findByDestination(String destination, int _start, int _end, String sortBy);

    List<Trip> findByEarliestDepartureTime(String localDateTime, int _start, int _end, String sortBy);

    List<Trip> findByLatestDepartureTime(String localDateTime, int _start, int _end, String sortBy);

    List<Trip> findByAvailablePlaces(int availablePlaces, int _start, int _end, String sortBy);

    List<Trip> findByLimitations(String pets, String smoking, String luggage, int _start, int _end, String sortBy);

    void changeTripStatus(long tripID, String status, User user);

    Trip findById(long id);

    void apply(long tripID, User user);

    void leaveTrip(long tripID, User user);

    void changePassengerStatus(long tripId, long passengerId, String status, User user);

    void rateDriver(long tripId, double rating, User user);

    void ratePassenger(long tripId, long passengerId, double rating, User user);

    void editTrip(Trip editTrip, User user);
}
