package com.company.carpoolingapplication.services.contracts;

import com.company.carpoolingapplication.models.Comment;
import com.company.carpoolingapplication.models.User;

public interface CommentService {
    void addComment(long tripID, Comment comment, User user);
}
