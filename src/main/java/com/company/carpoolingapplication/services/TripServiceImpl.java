package com.company.carpoolingapplication.services;

import com.company.carpoolingapplication.models.*;
import com.company.carpoolingapplication.models.common.PassengerStatus;
import com.company.carpoolingapplication.models.common.TripStatus;
import com.company.carpoolingapplication.repositories.PassengerDetailsRepository;
import com.company.carpoolingapplication.repositories.TripRepository;
import com.company.carpoolingapplication.repositories.UserRepository;
import com.company.carpoolingapplication.services.contracts.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TripServiceImpl implements TripService {
    private TripRepository repository;
    private UserRepository userRepository;
    private PassengerDetailsRepository passengerDetailsRepository;

    @Autowired
    public TripServiceImpl(TripRepository repository, UserRepository userRepository, PassengerDetailsRepository passengerDetailsRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.passengerDetailsRepository = passengerDetailsRepository;
    }

    @Override
    public void createTrip(Trip trip, User user) {
        trip.setDriver(user);
        repository.save(trip);
    }

    @Override
    public List<Trip> findAll(int _start, int _end, String sortBy) {
        if (sortBy.equals("")) {
            return repository.findAll(PageRequest.of(_start, _end)).getContent();
        } else {
            return repository.findAll(PageRequest.of(_start, _end, Sort.by(sortBy))).getContent();
        }
    }

    @Override
    public List<Trip> findByDriver(String username, int _start, int _end, String sortBy) {
        User user;
        user = userRepository.findByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format("Driver with username %s does not exist!", username));
        }
        if (sortBy.equals("")) {
            return repository.findByDriver(user, PageRequest.of(_start, _end));
        } else {
            return repository.findByDriver(user, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }
    }

    @Override
    public List<Trip> findByDriver(String username) {
        User user;
        user = userRepository.findByUsername(username);
        if (user == null) {
            return new ArrayList<>();
        }
        return repository.findByDriver(user);
    }

    @Override
    public List<Trip> findByOrigin(String origin, int _start, int _end, String sortBy) {
        if (sortBy.equals("")) {
            return repository.findByOrigin(origin, PageRequest.of(_start, _end));
        } else {
            return repository.findByOrigin(origin, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }
    }

    @Override
    public List<Trip> findByStatus(TripStatus status, int _start, int _end, String sortBy) {
        if (sortBy.equals("")) {
            return repository.findByStatus(status, PageRequest.of(_start, _end));
        } else {
            return repository.findByStatus(status, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }
    }

    @Override
    public List<Trip> findByDestination(String destination, int _start, int _end, String sortBy) {
        if (sortBy.equals("")) {
            return repository.findByDestination(destination, PageRequest.of(_start, _end));
        } else {
            return repository.findByDestination(destination, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }

    }

    @Override
    public List<Trip> findByEarliestDepartureTime(String localDateTime, int _start, int _end, String sortBy) {
        if (localDateTime == null || localDateTime.equals("")) {
            return new ArrayList<>();
        }
        LocalDateTime earliestDepartureTime = LocalDateTime.parse(localDateTime);
        if (sortBy.equals("")) {
            return repository.findByEarliestDepartureTime(earliestDepartureTime, PageRequest.of(_start, _end));
        } else {
            return repository.findByEarliestDepartureTime(earliestDepartureTime, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }
    }

    @Override
    public List<Trip> findByLatestDepartureTime(String localDateTime, int _start, int _end, String sortBy) {
        if (localDateTime == null || localDateTime.equals("")) {
            return new ArrayList<>();
        }
        LocalDateTime latestDepartureTime = LocalDateTime.parse(localDateTime);
        if (sortBy.equals("")) {
            return repository.findByLatestDepartureTime(latestDepartureTime, PageRequest.of(_start, _end));
        } else {
            return repository.findByLatestDepartureTime(latestDepartureTime, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }
    }

    @Override
    public List<Trip> findByAvailablePlaces(int availablePlaces, int _start, int _end, String sortBy) {
        List<Trip> trips = repository.findAll().stream().filter(p -> p.getAvailablePlaces() >= availablePlaces).collect(Collectors.toList());
        List<Trip> result = new ArrayList<>();
        if (sortBy.equals("departureTime")) {
            trips = trips.stream().sorted(Comparator.comparing(Trip::getDepartureTime)).collect(Collectors.toList());
        }
        if (sortBy.equals("origin")) {
            trips = trips.stream().sorted(Comparator.comparing(Trip::getOrigin)).collect(Collectors.toList());
        }
        if (sortBy.equals("destination")) {
            trips = trips.stream().sorted(Comparator.comparing(Trip::getDestination)).collect(Collectors.toList());
        }

        for (int i = _start * _end; i < _start * _end + _end; i++) {
            if (i >= trips.size()) {
                break;
            }
            result.add(trips.get(i));
        }

        return result;
    }


    @Override
    public List<Trip> findByLimitations(String pets, String smoking, String luggage, int _start, int _end, String sortBy) {
        boolean petsAsBoolean = pets.toLowerCase().equals("true");
        boolean smokingAsBoolean = smoking.toLowerCase().equals("true");
        boolean luggageAsBoolean = luggage.toLowerCase().equals("true");
        if (sortBy.equals("")) {
            return repository.findByLimitations(petsAsBoolean, smokingAsBoolean, luggageAsBoolean, PageRequest.of(_start, _end));
        } else {
            return repository.findByLimitations(petsAsBoolean, smokingAsBoolean, luggageAsBoolean, PageRequest.of(_start, _end, Sort.by(sortBy)));
        }
    }

    @Override
    public void changeTripStatus(long tripID, String status, User user) {
        if (repository.findById(tripID) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripID));
        }
        Trip trip = repository.findById(tripID);
        if (!userRepository.findByUsername(trip.getDriver().getUsername()).getUsername().equals(user.getUsername())) {
            throw new IllegalArgumentException("Only the driver can change trip's status!");
        }

        if (trip.getStatus().toString().equalsIgnoreCase("Done")) {
            throw new IllegalArgumentException("This trip is already finished! You can't change it anymore!");
        }
        if (trip.getStatus().toString().equalsIgnoreCase("Ongoing")
                && (!status.equalsIgnoreCase("Done"))) {
            throw new IllegalArgumentException("After trip started you can set status to \"Done\"");
        }
        if (trip.getStatus().toString().equalsIgnoreCase("Canceled")) {
            throw new IllegalArgumentException("This trip is already closed!");
        }

        if (trip.getStatus().toString().equalsIgnoreCase("Available") && !status.equalsIgnoreCase("Booked") &&
                trip.getStatus().toString().equalsIgnoreCase("Available") && !status.equalsIgnoreCase("Canceled")) {
            throw new IllegalArgumentException("You can change status only to \"Booked\" or \"Canceled\"!");
        }
        if (trip.getStatus().toString().equalsIgnoreCase("Booked") && (!status.equalsIgnoreCase("Ongoing") &&
                trip.getStatus().toString().equalsIgnoreCase("Booked") && (!status.equalsIgnoreCase("Canceled")))) {
            throw new IllegalArgumentException("You should change status to \"Ongoing\" or \"Canceled\"!");
        }
        trip.setStatus(TripStatus.valueOf(status.toUpperCase()));
        repository.save(trip);
    }

    @Override
    public Trip findById(long id) {
        if (repository.findById(id) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", id));
        }
        Trip trip = repository.findById(id);
        if (passengerDetailsRepository.getRatingAsDriver(trip.getDriver()) != null) {
            double rating = passengerDetailsRepository.getRatingAsDriver(trip.getDriver());
            trip.getDriver().setAverageRatingAsDriver(rating);
        }
        for (PassengerDetails pD : trip.getPassengerDetails()) {
            User passenger = pD.getUser();
            if (passengerDetailsRepository.getRatingAsPassenger(passenger) != null) {
                double rating = passengerDetailsRepository.getRatingAsPassenger(passenger);
                passenger.setAverageRatingAsPassenger(rating);
            }
        }
        return trip;
    }

    @Override
    public void apply(long tripID, User user) {
        if (repository.findById(tripID) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripID));
        }
        Trip trip = repository.findById(tripID);
        if (trip.getPassengerDetails().stream()
                .anyMatch(u -> u.getUser().getUsername().equals(user.getUsername()))) {
            throw new IllegalArgumentException(
                    String.format("User with username %s is already in this trip!", user.getUsername()));
        }
        if (trip.getDriver().getId() == user.getId()) {
            throw new IllegalArgumentException("You are the driver of this trip!");
        }
        if (trip.getAvailablePlaces() == 0) {
            throw new IllegalArgumentException("This trip is full! It does not have available places anymore!");
        }


        if (!trip.getStatus().toString().equalsIgnoreCase("Available")) {
            throw new IllegalArgumentException("This trip has already started!");
        }

        PassengerDetails passenger = new PassengerDetails();
        passenger.setUser(user);
        passenger.setTrip(trip);
        passenger = passengerDetailsRepository.save(passenger);
        trip.addPassenger(passenger);
        repository.save(trip);
    }


    @Override
    public void leaveTrip(long tripID, User user) {
        if (repository.findById(tripID) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripID));
        }
        Trip trip = repository.findById(tripID);
        if (trip.getDriver().getId() == user.getId()) {
            throw new IllegalArgumentException("You are the driver of this trip! You can't leave!");
        }
        if (trip.getPassengerDetails().stream()
                .noneMatch(u -> u.getUser().getUsername().equals(user.getUsername()))) {
            throw new IllegalArgumentException(
                    String.format("User with username %s is not in this trip!", user.getUsername()));
        }

        PassengerDetails passenger = passengerDetailsRepository.findById(new PassengerDetails.PassengerPK(user.getId(), tripID)).get();
        passengerDetailsRepository.delete(passenger);
    }

    @Override
    public void changePassengerStatus(long tripId, long passengerId, String status, User user) {
        if (repository.findById(tripId) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripId));
        }
        Trip trip = repository.findById(tripId);
        if (trip.getDriver().getId() != user.getId()) {
            throw new IllegalArgumentException("You can't change passenger status unless you are the driver of the trip!");
        }
        if (trip.getStatus() != TripStatus.AVAILABLE){
            throw new IllegalArgumentException("You can't change passenger status after trip has started!");
        }

        PassengerDetails passengerDetails = passengerDetailsRepository.findById(new PassengerDetails.PassengerPK(passengerId, tripId)).get();
        passengerDetails.setStatus(PassengerStatus.valueOf(status.toUpperCase()));
        passengerDetailsRepository.save(passengerDetails);
    }

    @Override
    public void rateDriver(long tripId, double rating, User user) {
        if (repository.findById(tripId) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripId));
        }
        Trip trip = repository.findById(tripId);
        if (trip.getPassengerDetails().stream()
                .noneMatch(u -> u.getUser().getUsername().equals(user.getUsername()))) {
            throw new IllegalArgumentException(
                    ("You can't rate the driver unless you are a passenger in the trip"));
        }

        if (!trip.getStatus().toString().equalsIgnoreCase("Done")) {
            throw new IllegalArgumentException("You can rate the driver only after the trip is over!");
        }
        if (trip.getPassengerDetails().stream()
                .noneMatch(u -> u.getUser().getUsername().equals(user.getUsername()) &&
                        u.getStatus().toString().equalsIgnoreCase("Accepted"))) {
            throw new IllegalArgumentException(
                    ("You can't rate the driver unless you are accepted in the trip!"));

        }
        PassengerDetails passengerDetails = passengerDetailsRepository.findById(new PassengerDetails.PassengerPK(user.getId(), tripId)).get();
        passengerDetails.setRatingGivenByPassengerToDriver(rating);
        passengerDetailsRepository.save(passengerDetails);
    }

    @Override
    public void ratePassenger(long tripId, long passengerId, double rating, User user) {
        if (repository.findById(tripId) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", tripId));
        }
        Trip trip = repository.findById(tripId);
        if (trip.getDriver().getId() != user.getId()) {
            throw new IllegalArgumentException("You can't rate passenger unless you are the driver of the trip!");
        }
        if (!trip.getStatus().toString().equalsIgnoreCase("Done")) {
            throw new IllegalArgumentException("You can rate a passenger only after the trip is over!");
        }
        PassengerDetails passengerDetails = passengerDetailsRepository.findById(new PassengerDetails.PassengerPK(passengerId, tripId)).get();
        if (!passengerDetails.getStatus().toString().equalsIgnoreCase("Accepted")) {
            throw new IllegalArgumentException("You can rate only accepted passengers!");
        }
        passengerDetails.setRatingGivenByDriverToPassenger(rating);
        passengerDetailsRepository.save(passengerDetails);
        repository.findById(tripId);
    }


    @Override
    public void editTrip(Trip trip, User user) {
        if (repository.findById(trip.getId()) == null) {
            throw new IllegalArgumentException(String.format("Trip with ID %d does not exist!", trip.getId()));
        }
        Trip t = repository.findById(trip.getId());

        if (t.getDriver().getId() != user.getId()) {
            throw new IllegalArgumentException("You can't edit trip unless you are the driver of the trip!");
        }
        if (!t.getStatus().toString().equalsIgnoreCase("Available")) {
            throw new IllegalArgumentException("Trip already started! You can't update it anymore!");
        }
        if (trip.getCarModel() != (null)) {
            t.setCarModel(trip.getCarModel());
        }
        if (trip.getMessage() != (null)) {
            t.setMessage(trip.getMessage());
        }
        if ((trip.getDepartureTime() != null)) {

            t.setDepartureTime(trip.getDepartureTime());
        }
        if (trip.getOrigin() != null) {
            t.setOrigin(trip.getOrigin());
        }
        if (trip.getDestination() != (null)) {
            t.setDestination(trip.getDestination());
        }
       if (trip.getAvailablePlaces() >= 0&&trip.getAvailablePlaces()>=t.getPassengerDetails().size()) {
          t.setAvailablePlaces(trip.getAvailablePlaces());
        }else {
           throw new IllegalArgumentException("available places");
       }
        if (trip.getDestination() != (null)) {
            t.setDestination(trip.getDestination());
        }
        t.setSmoking(trip.isSmoking());
        t.setPets(trip.isPets());
        t.setLuggage(trip.isLuggage());
        repository.save(t);
    }
}