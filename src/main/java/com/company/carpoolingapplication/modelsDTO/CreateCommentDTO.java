package com.company.carpoolingapplication.modelsDTO;


public class CreateCommentDTO {
    private String message;

    public CreateCommentDTO() {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
