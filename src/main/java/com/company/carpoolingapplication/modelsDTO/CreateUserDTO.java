package com.company.carpoolingapplication.modelsDTO;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CreateUserDTO {
    @NotBlank(message = "The field for username cannot be blank!")
    @Size(min = 1, max = 50, message = "Username should be between 1 and 50 symbols!")
    private String username;

    @NotBlank(message = "The field for first name cannot be blank!")
    @Size(min = 1, max = 255, message = "First name should be between 1 and 255 symbols!")
    private String firstName;

    @NotBlank(message = "The field for last name cannot be blank!")
    @Size(min = 1, max = 255, message = "Last name should be between 1 and 255 symbols!")
    private String lastName;

    @NotBlank(message = "The field for email cannot be blank!")
    @Email(message = "Email should be valid!")
    private String email;

    @NotBlank(message = "The field for password cannot be blank!")
    @Size(min = 4, message = "Password length cannot be less than 4 symbols!")
    private String password;

    @NotBlank(message = "The field for phone cannot be blank!")
    @Digits(integer = 15, fraction = 0, message = "Phone number should contain only digits! The maximum length is 20 digits")
    @Size(min = 1, max = 255, message = "Phone number should be between 1 and 50 symbols!")
    private String phone;

    public CreateUserDTO() {
    }


    public CreateUserDTO(String username, String firstName, String lastName,
                         String email, String password, String phone) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
