package com.company.carpoolingapplication.modelsDTO;

import java.util.List;

public class TripDTO {
    private long id;
    private UserDTO driver;
    private String carModel;
    private String message;
    private String departureTime;
    private String origin;
    private String destination;
    private int availablePlaces;
    private String status;
    private boolean isSmokingAllowed;
    private boolean arePetsAllowed;
    private boolean isLuggageAllowed;
    private List<PassengerDTO> passengers;
    private List<CommentDTO> comments;

    public TripDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserDTO getDriver() {
        return driver;
    }

    public void setDriver(UserDTO driver) {
        this.driver = driver;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isSmokingAllowed() {
        return isSmokingAllowed;
    }

    public void setSmokingAllowed(boolean smokingAllowed) {
        isSmokingAllowed = smokingAllowed;
    }

    public boolean isArePetsAllowed() {
        return arePetsAllowed;
    }

    public void setArePetsAllowed(boolean arePetsAllowed) {
        this.arePetsAllowed = arePetsAllowed;
    }

    public boolean isLuggageAllowed() {
        return isLuggageAllowed;
    }

    public void setLuggageAllowed(boolean luggageAllowed) {
        isLuggageAllowed = luggageAllowed;
    }

    public List<PassengerDTO> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengerDTO> passengers) {
        this.passengers = passengers;
    }

    public List<CommentDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDTO> comments) {
        this.comments = comments;
    }
}
