package com.company.carpoolingapplication.modelsDTO;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserDTO {
    private long id;

    private String username;

    @NotBlank(message = "The field for first name cannot be blank!")
    @Size(min = 1, max = 255, message = "First name should be between 1 and 255 symbols!")
    private String firstName;

    @NotBlank(message = "The field for last name cannot be blank!")
    @Size(min = 1, max = 255, message = "Last name should be between 1 and 255 symbols!")
    private String lastName;

    @NotBlank(message = "The field for email cannot be blank!")
    @Email(message = "Email should be valid!")
    private String email;

    @NotBlank(message = "The field for phone cannot be blank!")
    @Digits(integer = 15, fraction = 0, message = "Phone number should contain only digits! The maximum length is 20 digits")
    private String phone;

    private double ratingAsDriver;
    private double ratingAsPassenger;
    private String avatarUri;

    public UserDTO() {
    }

    public UserDTO(long id, String username) {
        this.id = id;
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getRatingAsDriver() {
        return ratingAsDriver;
    }

    public void setRatingAsDriver(double ratingAsDriver) {
        this.ratingAsDriver = ratingAsDriver;
    }

    public double getRatingAsPassenger() {
        return ratingAsPassenger;
    }

    public void setRatingAsPassenger(double ratingAsPassenger) {
        this.ratingAsPassenger = ratingAsPassenger;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }
}
