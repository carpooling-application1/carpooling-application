package com.company.carpoolingapplication.modelsDTO;

import com.company.carpoolingapplication.models.Comment;
import com.company.carpoolingapplication.models.PassengerDetails;
import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.models.common.TripStatus;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ModelsMapper {
    public static User fromCreateUserDTOtoUser(CreateUserDTO dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setEmail(dto.getEmail());
        user.setPassword(dto.getPassword());
        user.setPhone(dto.getPhone());
        return user;
    }


    public static Trip fromEditTripDTOtoTrip(EditTripDTO dto) {
        Trip trip = new Trip();

        trip.setId(dto.getId());
        trip.setCarModel(dto.getCarModel());
        trip.setMessage(dto.getMessage());

        LocalDateTime departureTime = LocalDateTime.parse(dto.getDepartureTime());
        LocalDateTime now = LocalDateTime.parse(Instant.now().toString().substring(0, 16));

        if (departureTime.isAfter(now)) {
            trip.setDepartureTime(departureTime);
        } else {
            throw new IllegalArgumentException("The departure time cannot be in the past!");
        }

        trip.setOrigin(dto.getOrigin());
        trip.setDestination(dto.getDestination());
        trip.setAvailablePlaces(dto.getAvailablePlaces());
        trip.setSmoking(dto.isSmoking());
        trip.setPets(dto.isPets());
        trip.setLuggage(dto.isLuggage());
        return trip;
    }


    public static Trip fromCreateTripDTOtoTrip(CreateTripDTO dto) {
        Trip trip = new Trip();
        trip.setCarModel(dto.getCarModel());
        trip.setMessage(dto.getMessage());

        LocalDateTime departureTime = LocalDateTime.parse(dto.getDepartureTime());
        LocalDateTime now = LocalDateTime.parse(Instant.now().toString().substring(0, 16));
        if (departureTime.isAfter(now)) {
            trip.setDepartureTime(departureTime);
        } else {
            throw new IllegalArgumentException("The departure time cannot be in the past!");
        }
        trip.setStatus(TripStatus.AVAILABLE);
        trip.setOrigin(dto.getOrigin());
        trip.setDestination(dto.getDestination());
        trip.setAvailablePlaces(dto.getAvailablePlaces());
        trip.setSmoking(dto.isSmoking());
        trip.setPets(dto.isPets());
        trip.setLuggage(dto.isLuggage());
        return trip;
    }

    public static PassengerDTO fromPassengerToPassengerDTO(PassengerDetails passengerDetails) {
        PassengerDTO passengerDTO = new PassengerDTO();
        passengerDTO.setUserId(passengerDetails.getUser().getId());
        passengerDTO.setUsername(passengerDetails.getUser().getUsername());
        passengerDTO.setFirstName(passengerDetails.getUser().getFirstName());
        passengerDTO.setLastName(passengerDetails.getUser().getLastName());
        passengerDTO.setEmail(passengerDetails.getUser().getEmail());
        passengerDTO.setPhone(passengerDetails.getUser().getPhone());
        passengerDTO.setRatingAsPassenger(passengerDetails.getUser().getAverageRatingAsPassenger());
        passengerDTO.setStatus(passengerDetails.getStatus().toString());
        passengerDTO.setAvatarUri(passengerDetails.getUser().getAvatarUri());
        return passengerDTO;
    }

    public static UserDTO fromUserToUserDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setPhone(user.getPhone());
        userDTO.setAvatarUri(user.getAvatarUri());
        userDTO.setRatingAsDriver(Math.round(user.getAverageRatingAsDriver() * 100) / 100.0d);
        userDTO.setRatingAsPassenger(Math.round(user.getAverageRatingAsPassenger() * 100) / 100.0d);
        return userDTO;
    }

    public static List<UserDTO> fromUserListToUserDTOList(List<User> users) {
        List<UserDTO> userDTOs = new ArrayList<>();
        for (User user : users) {
            userDTOs.add(fromUserToUserDTO(user));
        }
        return userDTOs;
    }

    public static List<PassengerDTO> fromPassengerListToPassengerDTOList
            (List<PassengerDetails> passengerDetailsList) {
        List<PassengerDTO> passengerDTOList = new ArrayList<>();
        for (PassengerDetails passengerDetails : passengerDetailsList) {
            passengerDTOList.add(ModelsMapper.fromPassengerToPassengerDTO(passengerDetails));
        }
        return passengerDTOList;
    }

    public static CommentDTO fromCommentToCommentDTO(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setMessage(comment.getMessage());
        commentDTO.setAuthor(ModelsMapper.fromUserToUserDTO(comment.getAuthor()));
        return commentDTO;
    }

    public static List<CommentDTO> fromCommentListToCommentDTOList(List<Comment> comments) {
        List<CommentDTO> commentDTOList = new ArrayList<>();
        for (Comment comment : comments) {
            commentDTOList.add(ModelsMapper.fromCommentToCommentDTO(comment));
        }
        return commentDTOList;
    }

    public static TripDTO fromTripToTripDTO(Trip trip) {
        TripDTO tripDTO = new TripDTO();
        tripDTO.setId(trip.getId());
        tripDTO.setDriver(ModelsMapper.fromUserToUserDTO(trip.getDriver()));
        tripDTO.setCarModel(trip.getCarModel());
        tripDTO.setMessage(trip.getMessage());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        tripDTO.setDepartureTime(trip.getDepartureTime().format(formatter));
        tripDTO.setOrigin(trip.getOrigin());
        tripDTO.setDestination(trip.getDestination());
        tripDTO.setAvailablePlaces(trip.getAvailablePlaces());
        tripDTO.setPassengers(ModelsMapper.fromPassengerListToPassengerDTOList(trip.getPassengerDetails()));
        tripDTO.setStatus(trip.getStatus().toString());
        tripDTO.setComments(ModelsMapper.fromCommentListToCommentDTOList(trip.getComments()));
        tripDTO.setSmokingAllowed(trip.isSmoking());
        tripDTO.setArePetsAllowed(trip.isPets());
        tripDTO.setLuggageAllowed(trip.isLuggage());
        return tripDTO;
    }

    public static List<TripDTO> fromTripListToTripDTOList(List<Trip> trips) {
        List<TripDTO> tripDTOList = new ArrayList<>();
        for (Trip trip : trips) {
            tripDTOList.add(ModelsMapper.fromTripToTripDTO(trip));
        }
        return tripDTOList;
    }

    public static JWTToken fromTokenToTokenDTO(String token) {
        JWTToken jwtToken = new JWTToken();
        jwtToken.setId_token(token);
        return jwtToken;
    }

    public static EditTripDTO fromTripToEditTrip(Trip trip) {
        EditTripDTO t = new EditTripDTO();
        t.setId(trip.getId());
        t.setCarModel(trip.getCarModel());
        t.setMessage(trip.getMessage());
        t.setDepartureTime(trip.getDepartureTime().toString());
        t.setOrigin(trip.getOrigin());
        t.setDestination(trip.getDestination());
        t.setAvailablePlaces(trip.getAvailablePlaces());
        t.setSmoking(trip.isSmoking());
        t.setPets(trip.isPets());
        t.setLuggage(trip.isLuggage());
        return t;
    }

    public static Comment fromCreateCommentDTOtoComment(CreateCommentDTO dto) {
        Comment comment = new Comment();
        comment.setMessage(dto.getMessage());
        return comment;
    }
}
