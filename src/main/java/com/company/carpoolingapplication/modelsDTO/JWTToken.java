package com.company.carpoolingapplication.modelsDTO;

public class JWTToken {
    private String id_token;

    public JWTToken() {
    }

    public String getId_token() {
        return id_token;
    }

    public void setId_token(String id_token) {
        this.id_token = id_token;
    }
}
