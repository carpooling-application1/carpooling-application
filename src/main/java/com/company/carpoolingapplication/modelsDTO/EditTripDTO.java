package com.company.carpoolingapplication.modelsDTO;

import javax.validation.constraints.*;

public class EditTripDTO {
    private long id;
    @Size(max = 255, message = "The length of the car model should be between 1 and 255 symbols!")
    private String carModel;

    @Size(max = 255, message = "The length of the message should be between 1 and 255 symbols!")
    private String message;

    @NotBlank(message = "The field for departure time cannot be blank!")
    private String departureTime;

    @NotBlank(message = "The field of the origin city cannot be blank!")
    @Size(max = 255, message = "The length of the origin city should be between 1 and 255 symbols!")
    private String origin;

    @NotBlank(message = "The field of the destination city cannot be blank!")
    @Size(max = 255, message = "The length of the destination city should be between 1 and 255 symbols!")
    private String destination;

    @NotNull(message = "The field for available places cannot be blank!")
    @PositiveOrZero
    private int availablePlaces;

    private boolean smoking;

    private boolean pets;

    private boolean luggage;

    public EditTripDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        return availablePlaces;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isPets() {
        return pets;
    }

    public void setPets(boolean pets) {
        this.pets = pets;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }
}
