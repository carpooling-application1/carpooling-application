package com.company.carpoolingapplication.repositories;

import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.models.common.TripStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TripRepository extends JpaRepository<Trip, Long> {
    Trip findById(long id);

    List<Trip> findByDriver(User driver, Pageable pageable);

    List<Trip> findByDriver(User driver);

    List<Trip> findByOrigin(String origin, Pageable pageable);

    List<Trip> findByDestination(String destination, Pageable pageable);

    List<Trip> findByStatus(TripStatus status, Pageable pageable);

    @Query("SELECT t FROM Trip t WHERE t.departureTime>=?1")
    List<Trip> findByEarliestDepartureTime(LocalDateTime localDateTime, Pageable pageable);

    @Query("SELECT t FROM Trip t WHERE t.departureTime<=?1")
    List<Trip> findByLatestDepartureTime(LocalDateTime localDateTime, Pageable pageable);

    @Query("SELECT t FROM Trip t WHERE t.pets=?1 AND t.smoking=?2 AND t.luggage=?3")
    List<Trip> findByLimitations(boolean pets, boolean smoking, boolean luggage, Pageable pageable);
}
