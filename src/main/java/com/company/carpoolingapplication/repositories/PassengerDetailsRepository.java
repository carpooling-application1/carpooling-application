package com.company.carpoolingapplication.repositories;

import com.company.carpoolingapplication.models.PassengerDetails;
import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PassengerDetailsRepository extends JpaRepository<PassengerDetails, PassengerDetails.PassengerPK> {
    @Query("SELECT AVG (ratingGivenByPassengerToDriver) FROM PassengerDetails " +
            "WHERE trip.driver=?1 AND status=1 and trip.status=3 AND ratingGivenByPassengerToDriver<>0")
    Double getRatingAsDriver(User user);

    @Query("SELECT AVG (ratingGivenByDriverToPassenger) FROM PassengerDetails " +
            "WHERE passenger=?1 AND ratingGivenByDriverToPassenger<>0 and status=1")
    Double getRatingAsPassenger(User user);

    @Query("SELECT DISTINCT p.trip FROM PassengerDetails p WHERE p.passenger=?1")
    List<Trip> getTripsOfUser(User user);
}
