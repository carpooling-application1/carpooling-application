package com.company.carpoolingapplication.models.common;

public enum TripStatus {
    AVAILABLE,
    BOOKED,
    ONGOING,
    DONE,
    CANCELED;

    @Override
    public String toString() {
        switch (this) {
            case AVAILABLE:
                return "Available";
            case BOOKED:
                return "Booked";
            case ONGOING:
                return "Ongoing";
            case DONE:
                return "Done";
            case CANCELED:
                return "Canceled";
            default:
                throw new IllegalArgumentException("There is no such trip status!");
        }
    }
}

