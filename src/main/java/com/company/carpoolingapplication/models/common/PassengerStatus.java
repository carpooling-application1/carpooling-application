package com.company.carpoolingapplication.models.common;

public enum PassengerStatus {
    PENDING,
    ACCEPTED,
    REJECTED,
    CANCELED,
    ABSENT;

    @Override
    public String toString() {
        switch (this) {
            case PENDING:
                return "Pending";
            case ACCEPTED:
                return "Accepted";
            case REJECTED:
                return "Rejected";
            case ABSENT:
                return "Absent";
            case CANCELED:
                return "Canceled";
            default:
                throw new IllegalArgumentException("There is no such passenger status!");
        }
    }
}

