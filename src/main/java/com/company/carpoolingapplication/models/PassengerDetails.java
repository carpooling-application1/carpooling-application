package com.company.carpoolingapplication.models;

import com.company.carpoolingapplication.models.common.PassengerStatus;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "passenger_details")
@IdClass(PassengerDetails.PassengerPK.class)
public class PassengerDetails {
    @Id
    @ManyToOne
    @JoinColumn(name = "passenger")
    private User passenger;

    @Id
    @ManyToOne
    @JoinColumn(name = "trip")
    private Trip trip;

    @Column(name = "status")
    private PassengerStatus status = PassengerStatus.PENDING;

    @Column(name = "ratingGivenByDriver")
    private double ratingGivenByDriverToPassenger;

    @Column(name = "ratingGivenByPassenger")
    private double ratingGivenByPassengerToDriver;

    public PassengerDetails() {
    }

    public User getUser() {
        return passenger;
    }

    public void setUser(User user) {
        this.passenger = user;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public PassengerStatus getStatus() {
        return status;
    }

    public void setStatus(PassengerStatus status) {
        this.status = status;
    }

    public double getRatingGivenByDriverToPassenger() {
        return ratingGivenByDriverToPassenger;
    }

    public void setRatingGivenByDriverToPassenger(double ratingGivenByDriverToPassenger) {
        this.ratingGivenByDriverToPassenger = ratingGivenByDriverToPassenger;
    }

    public double getRatingGivenByPassengerToDriver() {
        return ratingGivenByPassengerToDriver;
    }

    public void setRatingGivenByPassengerToDriver(double ratingGivenByPassengerToDriver) {
        this.ratingGivenByPassengerToDriver = ratingGivenByPassengerToDriver;
    }


    public static class PassengerPK implements Serializable {
        protected Long passenger;
        protected Long trip;

        public PassengerPK() {
        }

        public PassengerPK(Long passenger, Long trip) {
            this.passenger = passenger;
            this.trip = trip;
        }

        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }
    }
}

