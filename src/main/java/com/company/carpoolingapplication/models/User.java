package com.company.carpoolingapplication.models;

import com.company.carpoolingapplication.models.common.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "username", length = 50)
    private String username;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email")
    private String email;

    @NotBlank
    @Column(name = "password")
    private String password;

    @Column(name = "phone")
    private String phone;

    @Column(name = "avatarUri")
    private String avatarUri = "../img/default-avatar.jpg";

    @OneToMany(mappedBy = "author")
    @JsonIgnore
    private List<Comment> userComments = new ArrayList<>();

    @OneToMany(mappedBy = "passenger")
    @JsonIgnore
    private List<PassengerDetails> tripsAsPassenger = new ArrayList<>();

    @OneToMany(mappedBy = "driver")
    @JsonIgnore
    private List<Trip> tripsAsDriver = new ArrayList<>();

    @ElementCollection(fetch = FetchType.EAGER)
    List<Role> roles = new ArrayList<>();

    @Transient
    private double averageRatingAsPassenger;

    @Transient
    private double averageRatingAsDriver;


    public User() {
    }

    public User(long id, String username) {
        this.id = id;
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatarUri() {
        return avatarUri;
    }

    public void setAvatarUri(String avatarUri) {
        this.avatarUri = avatarUri;
    }

    public List<Comment> getComments() {
        return userComments;
    }

    public void setComments(List<Comment> comments) {
        this.userComments = comments;
    }

    public List<PassengerDetails> getTripsAsPassenger() {
        return tripsAsPassenger;
    }

    public void setTripsAsPassenger(List<PassengerDetails> tripsAsPassenger) {
        this.tripsAsPassenger = tripsAsPassenger;
    }

    public List<Comment> getUserComments() {
        return userComments;
    }

    public void setUserComments(List<Comment> userComments) {
        this.userComments = userComments;
    }

    public List<Trip> getTripsAsDriver() {
        return tripsAsDriver;
    }

    public void setTripsAsDriver(List<Trip> tripsAsDriver) {
        this.tripsAsDriver = tripsAsDriver;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void addRole(Role role) {
        roles.add(role);
    }

    public double getAverageRatingAsPassenger() {
        return averageRatingAsPassenger;
    }

    public void setAverageRatingAsPassenger(double averageRatingAsPassenger) {
        this.averageRatingAsPassenger = averageRatingAsPassenger;
    }

    public double getAverageRatingAsDriver() {
        return averageRatingAsDriver;
    }

    public void setAverageRatingAsDriver(double averageRatingAsDriver) {
        this.averageRatingAsDriver = averageRatingAsDriver;
    }
}
