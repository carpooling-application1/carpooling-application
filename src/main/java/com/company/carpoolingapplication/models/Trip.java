package com.company.carpoolingapplication.models;

import com.company.carpoolingapplication.models.common.PassengerStatus;
import com.company.carpoolingapplication.models.common.TripStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "trips")
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "carModel")
    private String carModel;

    @Column(name = "message")
    private String message;

    @Column(name = "departureTime")
    private LocalDateTime departureTime;

    @Column(name = "origin")
    private String origin;

    @Column(name = "destination")
    private String destination;

    @Column(name = "availablePlaces")
    private int availablePlaces;

    @Column(name = "status")
    private TripStatus status = TripStatus.AVAILABLE;

    @Column(name = "pets")
    private boolean pets;

    @Column(name = "luggage")
    private boolean luggage;

    @Column(name = "smoking")
    private boolean smoking;

    @ManyToOne
    @JoinColumn(name = "driver")
    @JsonIgnore
    private User driver;

    @OneToMany(mappedBy = "trip")
    @JsonIgnore
    private List<PassengerDetails> passengerDetails = new ArrayList<>();


    @OneToMany(mappedBy = "trip")
    @JsonIgnore
    private List<Comment> tripComments = new ArrayList<>();

    public Trip() {
    }


    public Trip(long id, User driver, String destination) {
        this.id = id;
        this.driver = driver;
        this.destination = destination;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getAvailablePlaces() {
        int acceptedPassengers = 0;
        for (PassengerDetails passenger : passengerDetails) {
            if (passenger.getStatus() == PassengerStatus.ACCEPTED) {
                acceptedPassengers++;
            }
        }
        return availablePlaces - acceptedPassengers;
    }

    public void setAvailablePlaces(int availablePlaces) {
        this.availablePlaces = availablePlaces;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }

    public boolean isPets() {
        return pets;
    }

    public void setPets(boolean pets) {
        this.pets = pets;
    }

    public boolean isLuggage() {
        return luggage;
    }

    public void setLuggage(boolean luggage) {
        this.luggage = luggage;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public List<PassengerDetails> getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(List<PassengerDetails> passengerDetails) {
        this.passengerDetails = passengerDetails;
    }

    public void addPassenger(PassengerDetails passenger) {
        passengerDetails.add(passenger);
    }

    public List<Comment> getComments() {
        return tripComments;
    }

    public void setComments(List<Comment> comments) {
        this.tripComments = comments;
    }


}
