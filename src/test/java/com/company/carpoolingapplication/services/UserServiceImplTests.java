package com.company.carpoolingapplication.services;


import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.modelsDTO.UserDTO;
import com.company.carpoolingapplication.repositories.PassengerDetailsRepository;
import com.company.carpoolingapplication.repositories.TripRepository;
import com.company.carpoolingapplication.repositories.UserRepository;
import com.company.carpoolingapplication.security.JwtTokenProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockRepository;
    @Mock
    PassengerDetailsRepository passengerDetailsRepository;
    @Mock
    TripRepository tripRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    JwtTokenProvider jwtTokenProvider;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getByName_Should_ReturnFromRepository_When_UserWithSameUsernameExist() {
        // Arrange
        Mockito.when(mockRepository.findByUsername("User"))
                .thenReturn(new User(1, "User"));

        // Act
        service.getByName("User");

        // Assert
        Mockito.verify(mockRepository, Mockito.times(2)).findByUsername("User");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByName_Should_ThrowException_When_UserWithSameUsernameNotExist() {
        // Arrange
        Mockito.when(mockRepository.findByUsername("User"))
                .thenReturn(null);

        // Act
        service.getByName("User");
    }

    @Test(expected = IllegalArgumentException.class)
    public void createUser_Should_ThrowException_When_UserWithSameUsernameExist() {
        // Arrange
        Mockito.when(mockRepository.findByUsername("User"))
                .thenReturn(null);

        // Act
        service.getByName("User");
    }

    @Test
    public void createUser_Should_SaveToRepository_When_UserWithSameUsernameNotExist() {
        User user = new User(1, "ivan.ivanov");
        Mockito.when(mockRepository.findByUsername("ivan.ivanov"))
                .thenReturn(null);

        //Act
        service.createUser(user);
//       user.addRole(Role.ROLE_CLIENT);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getImage_Should_ThrowException_When_UserWithIdNotExist() {
        // Arrange
        long id = 1;
        Mockito.when(mockRepository.findById(id))
                .thenReturn(Optional.empty());

        // Act
        service.getImage(id);
    }

    @Test
    public void getImage_Should_ReturnFromRepository_When_UserWithIdExist() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Mockito.when(mockRepository.findById(id))
                .thenReturn(Optional.of(user));

        // Act
        service.getImage(id);

        //
        Mockito.verify(mockRepository, Mockito.times(2)).findById(id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveImage_Should_ThrowException_When_UserWithIdNotExist() throws Exception {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        MultipartFile image = new MockMultipartFile("files", "filename.txt", "text/plain", "hello".getBytes(StandardCharsets.UTF_8));
        Mockito.when(mockRepository.findById(id))
                .thenReturn(Optional.empty());

        // Act
        service.saveImage(image, id, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveImage_Should_ThrowException_When_UserUploadPhotoOtherUser() throws Exception {
        // Arrange
        long id = 1;
        User user = new User(2, "User");
        User user1 = new User(1, "User1");
        MultipartFile image = new MockMultipartFile("files", "filename.txt", "text/plain", "hello".getBytes(StandardCharsets.UTF_8));
        Mockito.when(mockRepository.findById(id))
                .thenReturn(Optional.of(user1));

        // Act
        service.saveImage(image, id, user);
    }

    @Test
    public void saveImage_Should_SaveToRepository() throws Exception {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        MultipartFile image = new MockMultipartFile("files", "filename.txt", "text/plain", "hello".getBytes(StandardCharsets.UTF_8));
        Mockito.when(mockRepository.findById(id))
                .thenReturn(Optional.of(user));

        // Act
        service.saveImage(image, id, user);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);
    }


    @Test
    public void getTrips_Should_ReturnFromTripRepository() {
        // Arrange
        User user = new User(1, "User");
        Mockito.when(passengerDetailsRepository.getTripsOfUser(user))
                .thenReturn(new ArrayList<>());

        // Act
        service.getTrips(user, 2, 2, "destination");

        //
        Mockito.verify(tripRepository, Mockito.times(1)).findByDriver(user);
    }

    @Test
    public void getTrips_Should_ReturnFromTripRepositoryOrigin() {
        // Arrange
        User user = new User(1, "User");
        Mockito.when(passengerDetailsRepository.getTripsOfUser(user))
                .thenReturn(new ArrayList<>());

        // Act
        service.getTrips(user, 2, 2, "origin");

        //
        Mockito.verify(tripRepository, Mockito.times(1)).findByDriver(user);
    }

    @Test
    public void getTrips_Should_ReturnFromTripRepositoryDepartureTime() {
        // Arrange
        User user = new User(1, "User");
        Mockito.when(passengerDetailsRepository.getTripsOfUser(user))
                .thenReturn(new ArrayList<>());

        // Act
        service.getTrips(user, 2, 2, "departureTime");

        //
        Mockito.verify(tripRepository, Mockito.times(1)).findByDriver(user);
    }

    @Test
    public void getTrips_Should_ReturnFromTripRepositoryRightAmountTrips() {
        // Arrange
        User user = new User(1, "User");
        Mockito.when(passengerDetailsRepository.getTripsOfUser(user))
                .thenReturn(Arrays.asList(new Trip(1, user, "Varna"),
                        new Trip(2, user, "Sofia")));
        Mockito.when(tripRepository.findByDriver(user))
                .thenReturn(new ArrayList<>());
        // Act
        List<Trip> result = service.getTrips(user, 0, 5, "destination");

        //
        Assert.assertEquals(2, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void editUser_Should_ThrowException_When_UserNotExist() {
        // Arrange
        User user = new User(1, "User");
        UserDTO dto = new UserDTO(2, "User");
        Mockito.when(mockRepository.findById(dto.getId()))
                .thenReturn(Optional.empty());

        // Act
        service.editUser(dto, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void editUser_Should_ThrowException_When_UserEditOtherUser() {
        // Arrange
        User user = new User(1, "User");
        UserDTO dto = new UserDTO(2, "User");
        Mockito.when(mockRepository.findById(dto.getId()))
                .thenReturn(Optional.of(user));

        // Act
        service.editUser(dto, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void editUser_Should_ThrowException_When_ChangeWithExistingUsername() {
        // Arrange
        User user = new User(1, "User");
        UserDTO dto = new UserDTO(1, "User1");
        Mockito.when(mockRepository.findById(dto.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(mockRepository.getOne(dto.getId()))
                .thenReturn(user);
        Mockito.when(mockRepository.findByUsername(dto.getUsername()))
                .thenReturn(new User(1, "User1"));

        // Act
        service.editUser(dto, user);
    }

    @Test
    public void editUser_Should_SaveToRepository() {
        // Arrange
        User user = new User(1, "User");
        UserDTO dto = new UserDTO(1, "User1");
        Mockito.when(mockRepository.findById(dto.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(mockRepository.getOne(dto.getId()))
                .thenReturn(user);


        // Act
        service.editUser(dto, user);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);
    }

    @Test
    public void editUser_Should_SaveToRepository_FullData() {
        // Arrange
        User user = new User(1, "User");
        UserDTO dto = new UserDTO(1, "User1");
        dto.setFirstName("Ivan");
        dto.setLastName("Ivanov");
        dto.setEmail("ivan@gmail.com");
        dto.setPhone("8888");
        dto.setUsername("ivan.ivanov");
        Mockito.when(mockRepository.findById(dto.getId()))
                .thenReturn(Optional.of(user));
        Mockito.when(mockRepository.getOne(dto.getId()))
                .thenReturn(user);


        // Act
        service.editUser(dto, user);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);
    }


    @Test
    public void getTopTenDrivers_Should_ReturnFromRepository() {
        // Arrange
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        // Act
        service.getTopTenDrivers();

        //
        Mockito.verify(mockRepository, Mockito.times(3)).findAll();

    }

    @Test
    public void refresh_Should_ReturnFromRepository() {
        // Arrange
        User user = new User(1, "User");
        Mockito.when(mockRepository.findByUsername("User"))
                .thenReturn(user);

        // Act
        service.refresh("User");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByUsername("User");
    }

}
