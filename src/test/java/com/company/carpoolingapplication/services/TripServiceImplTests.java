package com.company.carpoolingapplication.services;


import com.company.carpoolingapplication.models.PassengerDetails;
import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.models.common.PassengerStatus;
import com.company.carpoolingapplication.models.common.TripStatus;
import com.company.carpoolingapplication.repositories.PassengerDetailsRepository;
import com.company.carpoolingapplication.repositories.TripRepository;
import com.company.carpoolingapplication.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TripServiceImplTests {
    @Mock
    TripRepository mockRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    PassengerDetailsRepository passengerDetailsRepository;

    @InjectMocks
    TripServiceImpl service;

    @Test
    public void findAll_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        Mockito.when(mockRepository.findAll(PageRequest.of(start, end))).thenReturn(
                (new PageImpl<>(Arrays.asList(
                        new Trip(1, new User(), "Sofia"),
                        new Trip(2, new User(), "Sofia")
                ))));

        //Act
        service.findAll(start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findAll(PageRequest.of(start, end));
    }


    @Test
    public void findAll_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        Mockito.when(mockRepository.findAll(PageRequest.of(start, end, Sort.by(sort)))).thenReturn(
                (new PageImpl<>(Arrays.asList(
                        new Trip(1, new User(), "Sofia"),
                        new Trip(2, new User(), "Sofia")
                ))));

        //Act
        service.findAll(start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findAll(PageRequest.of(start, end, Sort.by(sort)));
    }

    @Test
    public void createTrip_Should_saveToRepository() {
        //Arrange
        User user = new User();
        Trip trip = new Trip();

        //Act
        service.createTrip(trip, user);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(trip);
    }

    @Test
    public void findById_Should_FindByIdFromRepository() {
        // Arrange
        long id = 1;
        Trip trip = new Trip();
        User driver = new User();
        trip.setDriver(driver);
        Mockito.when(mockRepository.findById(id)).thenReturn(
                trip);

        //Act
        service.findById(id);

        //
        Mockito.verify(mockRepository, Mockito.times(2)).findById(id);
    }

    @Test
    public void findById_Should_ReturnTripFromRepository() {
        // Arrange
        long id = 1;
        Trip trip = new Trip();
        User driver = new User();
        trip.setDriver(driver);
        Mockito.when(mockRepository.findById(id)).thenReturn(
                trip);

        //Act
        Trip result = service.findById(id);

        //
        Assert.assertEquals(trip, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findById_Should_Throw_Exception_When_TripNotExists() {
        // Arrange
        long id = 1;
        Mockito.when(mockRepository.findById(id)).thenReturn(
                null);

        //Act
        service.findById(id);
    }


    @Test(expected = IllegalArgumentException.class)
    public void findByDriver_Should_Throw_Exception_When_DriverNotExists() {
        // Arrange
        String username = "ivan.ivanov";
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(
                null);

        //Act
        service.findByDriver(username, start, end, sort);
    }


    @Test
    public void findByDriver1_Should_ReturnEmptyList_When_UserNotExists() {
        // Arrange
        String username = "ivan.ivanov";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(
                null);

        //Act
        List<Trip> result = service.findByDriver(username);

        //
        Assert.assertEquals(0, result.size());
    }


    @Test
    public void findByDriver_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String username = "ivan.ivanov";
        User user = new User(1, username);
        Mockito.when(userRepository.findByUsername(username))
                .thenReturn(user);

        //Act
        service.findByDriver(username, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByDriver(user, PageRequest.of(start, end));
    }

    @Test
    public void findByDriver1_Should_ReturnFromRepository() {
        // Arrange
        String username = "ivan.ivanov";
        User user = new User(1, username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(
                user);

        //Act
        service.findByDriver(username);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByDriver(user);
    }

    @Test
    public void findByDriver_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String username = "ivan.ivanov";
        String sort = "destination";
        User user = new User(1, username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(
                user);

        //Act
        service.findByDriver(username, start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByDriver(user, PageRequest.of(start, end, Sort.by(sort)));
    }


    @Test
    public void findByOrigin_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String origin = "Sofia";

        //Act
        service.findByOrigin(origin, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByOrigin(origin, PageRequest.of(start, end));
    }


    @Test
    public void findByOrigin_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        String origin = "Sofia";

        //Act
        service.findByOrigin(origin, start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByOrigin(origin, PageRequest.of(start, end, Sort.by(sort)));
    }


    @Test
    public void findByStatus_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        TripStatus status = TripStatus.AVAILABLE;

        //Act
        service.findByStatus(status, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByStatus(status, PageRequest.of(start, end));
    }


    @Test
    public void findByStatus_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        TripStatus status = TripStatus.AVAILABLE;

        //Act
        service.findByStatus(status, start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByStatus(status, PageRequest.of(start, end, Sort.by(sort)));
    }

    @Test
    public void findByDestination_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String destination = "Sofia";

        //Act
        service.findByDestination(destination, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByDestination(destination, PageRequest.of(start, end));
    }


    @Test
    public void findByDestination_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        String destination = "Sofia";

        //Act
        service.findByDestination(destination, start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByDestination(destination, PageRequest.of(start, end, Sort.by(sort)));
    }

    @Test
    public void findByEarliestDepartureTime_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String date = "2019-08-08T12:30";
        LocalDateTime dateTime = LocalDateTime.parse(date);

        //Act
        service.findByEarliestDepartureTime(date, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByEarliestDepartureTime(dateTime, PageRequest.of(start, end));
    }


    @Test
    public void findByEarliestDepartureTime_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        String date = "2019-08-08T12:30";
        LocalDateTime dateTime = LocalDateTime.parse(date);

        //Act
        service.findByEarliestDepartureTime(date, start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByEarliestDepartureTime(dateTime, PageRequest.of(start, end, Sort.by(sort)));
    }

    @Test
    public void findByEarliestDepartureTime_Should_ReturnEmptyList_When_DateIsEmpty() {
        // Arrange
        int start = 0;
        int end = 2;
        String date = "";

        //Act
        List<Trip> result = service.findByEarliestDepartureTime(date, start, end, "");

        //
        Assert.assertEquals(0, result.size());
    }


    @Test
    public void findByLatestDepartureTime_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String date = "2019-08-08T12:30";
        LocalDateTime dateTime = LocalDateTime.parse(date);

        //Act
        service.findByLatestDepartureTime(date, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByLatestDepartureTime(dateTime, PageRequest.of(start, end));
    }


    @Test
    public void findByLatestDepartureTime_Should_ReturnSortedFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String sort = "destiantion";
        String date = "2019-08-08T12:30";
        LocalDateTime dateTime = LocalDateTime.parse(date);

        //Act
        service.findByLatestDepartureTime(date, start, end, sort);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByLatestDepartureTime(dateTime, PageRequest.of(start, end, Sort.by(sort)));
    }

    @Test
    public void findByLatestDepartureTime_Should_ReturnEmptyList_When_DateIsEmpty() {
        // Arrange
        int start = 0;
        int end = 2;
        String date = "";

        //Act
        List<Trip> result = service.findByLatestDepartureTime(date, start, end, "");

        //
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void findByAvailablePlaces_Should_ReturnFromRepository() {
        //Act
        service.findByAvailablePlaces(2, 2, 2, "origin");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void findByLimitations_Should_ReturnFromRepository() {
        // Arrange
        int start = 0;
        int end = 2;
        String pets = "true";
        String smoking = "true";
        String luggage = "true";
        boolean petsAsBoolean = pets.toLowerCase().equals("true");
        boolean smokingAsBoolean = smoking.toLowerCase().equals("true");
        boolean luggageAsBoolean = luggage.toLowerCase().equals("true");


        //Act
        service.findByLimitations(pets, smoking, luggage, start, end, "");

        //
        Mockito.verify(mockRepository, Mockito.times(1)).findByLimitations(petsAsBoolean, smokingAsBoolean, luggageAsBoolean, PageRequest.of(start, end));
    }


    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_TripNotExist() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Mockito.when(mockRepository.findById(id)).thenReturn(null);

        //Act
        service.changeTripStatus(id, "Done", user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_UserNotDriver() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        User user1 = new User(2, "User1");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Done", user1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_StatusDone() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        trip.setStatus(TripStatus.DONE);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Available", user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_StatusOngoing() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        trip.setStatus(TripStatus.ONGOING);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Available", user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_StatusCanceled() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        trip.setStatus(TripStatus.CANCELED);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Available", user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_StatusAvailable() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        trip.setStatus(TripStatus.AVAILABLE);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Available", user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changeTripStatus_Should_ThrowException_When_StatusBooked() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        trip.setStatus(TripStatus.BOOKED);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Available", user);
    }

    @Test
    public void changeTripStatus_Should_SaveToRepository() {
        // Arrange
        long id = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        trip.setStatus(TripStatus.ONGOING);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);
        Mockito.when(userRepository.findByUsername("User")).thenReturn(user);

        //Act
        service.changeTripStatus(id, "Done", user);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(trip);
    }


    @Test(expected = IllegalArgumentException.class)
    public void apply_Should_ThrowException_When_TripNotExist() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");

        Mockito.when(mockRepository.findById(id)).thenReturn(null);

        //Act
        service.apply(tripId, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_Should_ThrowException_When_PassengerAlreadyInTrip() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        PassengerDetails passengerDetails = new PassengerDetails();
        passengerDetails.setUser(user);
        Trip trip = new Trip(1, user, "Sofia");
        trip.addPassenger(passengerDetails);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.apply(tripId, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_Should_ThrowException_When_DriverApply() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.apply(tripId, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void apply_Should_ThrowException_When_TripIsFull() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        User user1 = new User(2, "User1");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setAvailablePlaces(0);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.apply(tripId, user1);
    }


    @Test(expected = IllegalArgumentException.class)
    public void apply_Should_ThrowException_When_TipStatusIsNotAvailable() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        User user1 = new User(2, "User1");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setStatus(TripStatus.DONE);
        trip.setAvailablePlaces(5);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.apply(tripId, user1);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(trip);
    }

    @Test
    public void apply_Should_SaveToRepository() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        User user1 = new User(2, "User1");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setStatus(TripStatus.AVAILABLE);
        trip.setAvailablePlaces(5);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.apply(tripId, user1);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(trip);
    }

    @Test(expected = IllegalArgumentException.class)
    public void leaveTrip_Should_ThrowException_When_TripNotExist() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");

        Mockito.when(mockRepository.findById(id)).thenReturn(null);

        //Act
        service.leaveTrip(tripId, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void leaveTrip_Should_ThrowException_When_UserNotInTrip() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        User user1 = new User(2, "User1");
        Trip trip = new Trip(1, user, "Sofia");
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.leaveTrip(tripId, user1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void leaveTrip_Should_ThrowException_When_DriverLeave() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        trip.setDriver(user);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.leaveTrip(tripId, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePassengerStatus_Should_ThrowException_When_TripNotExist() {
        // Arrange
        long id = 1;
        long tripId = 1;
        long passengerId = 1;
        String status = "Accepted";
        User user = new User(1, "User");

        Mockito.when(mockRepository.findById(id)).thenReturn(null);

        //Act
        service.changePassengerStatus(tripId, passengerId, status, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void changePassengerStatus_Should_ThrowException_When_NotDriver() {
        // Arrange
        long id = 1;
        long tripId = 1;
        long passengerId = 1;
        String status = "Accepted";
        User user = new User(1, "User");
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.changePassengerStatus(tripId, passengerId, status, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rateDriver_Should_ThrowException_When_TripNotExist() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");

        Mockito.when(mockRepository.findById(id)).thenReturn(null);

        //Act
        service.rateDriver(tripId, 2.0, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rateDriver_Should_ThrowException_When_UserNotInTrip() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.rateDriver(tripId, 2.0, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rateDriver_Should_ThrowException_When_TripNotDone() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        trip.setStatus(TripStatus.ONGOING);
        PassengerDetails passengerDetails = new PassengerDetails();
        passengerDetails.setTrip(trip);
        passengerDetails.setUser(user);
        trip.addPassenger(passengerDetails);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.rateDriver(tripId, 2.0, user);
    }


    @Test(expected = IllegalArgumentException.class)
    public void rateDriver_Should_ThrowException_When_PassengerStatusNotAccepted() {
        // Arrange
        long id = 1;
        long tripId = 1;
        User user = new User(1, "User");
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        trip.setStatus(TripStatus.DONE);
        PassengerDetails passengerDetails = new PassengerDetails();
        passengerDetails.setTrip(trip);
        passengerDetails.setUser(user);
        passengerDetails.setStatus(PassengerStatus.PENDING);
        trip.addPassenger(passengerDetails);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.rateDriver(tripId, 2.0, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ratePassenger_Should_ThrowException_When_TripNotDone() {
        // Arrange
        long id = 1;
        long tripId = 1;
        long passengerId = 1;
        User user = new User(1, "User");
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        trip.setStatus(TripStatus.ONGOING);
        PassengerDetails passengerDetails = new PassengerDetails();
        passengerDetails.setTrip(trip);
        passengerDetails.setUser(user);
        trip.addPassenger(passengerDetails);
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.ratePassenger(tripId, passengerId, 2.0, driver);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ratePassenger_Should_ThrowException_When_TripNotExist() {
        // Arrange
        long id = 1;
        long tripId = 1;
        long passengerId = 1;
        User user = new User(1, "User");

        Mockito.when(mockRepository.findById(id)).thenReturn(null);

        //Act
        service.ratePassenger(tripId, passengerId, 2.0, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ratePassenger_Should_ThrowException_When_UserNotInTrip() {
        // Arrange
        long id = 1;
        long tripId = 1;
        long passengerId = 1;
        User user = new User(1, "User");
        User driver = new User(10, "User");
        Trip trip = new Trip(1, driver, "Sofia");
        Mockito.when(mockRepository.findById(id)).thenReturn(trip);

        //Act
        service.ratePassenger(tripId, passengerId, 2.0, user);
    }


    @Test(expected = IllegalArgumentException.class)
    public void editTrip_Should_ThrowException_When_TripNotExist() {
        // Arrange
        User user = new User(1, "User");
        Trip trip = new Trip(1, user, "Sofia");
        Mockito.when(mockRepository.findById(trip.getId()))
                .thenReturn(null);

        // Act
        service.editTrip(trip, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void editTrip_Should_ThrowException_When_DriverNotEdit() {
        // Arrange
        User user = new User(1, "User");
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        Mockito.when(mockRepository.findById(trip.getId()))
                .thenReturn(trip);

        // Act
        service.editTrip(trip, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void editTrip_Should_ThrowException_When_Status() {
        // Arrange
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        trip.setStatus(TripStatus.DONE);
        Mockito.when(mockRepository.findById(trip.getId()))
                .thenReturn(trip);

        // Act
        service.editTrip(trip, driver);
    }

    @Test
    public void editTrip_Should_SaveToRepository() {
        // Arrange
        User driver = new User(2, "User1");
        Trip trip = new Trip(1, driver, "Sofia");
        Mockito.when(mockRepository.findById(trip.getId()))
                .thenReturn(trip);

        // Act
        service.editTrip(trip, driver);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(trip);
    }
}
