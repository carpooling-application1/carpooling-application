package com.company.carpoolingapplication.services;

import com.company.carpoolingapplication.models.Comment;
import com.company.carpoolingapplication.models.Trip;
import com.company.carpoolingapplication.models.User;
import com.company.carpoolingapplication.repositories.CommentRepository;
import com.company.carpoolingapplication.repositories.TripRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTests {
    @Mock
    TripRepository tripRepository;
    @Mock
    CommentRepository mockRepository;


    @InjectMocks
    CommentServiceImpl service;

    @Test(expected = IllegalArgumentException.class)
    public void addComment_Should_Throw_exception_When_TripNotExists() {
        // Arrange
        long id = 1;
        Comment comment = new Comment();
        User user = new User();
        Mockito.when(tripRepository.findById(id)).thenReturn(
                null);

        //Act
        service.addComment(id, comment, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addComment_Should_Throw_exception_When_UserDoesNotParticipate() {
        // Arrange
        long id = 1;
        Comment comment = new Comment();
        User user = new User(2, "ivan.ivanov");
        Trip trip = new Trip();
        User driver = new User(1, "mertin.ivanov");
        trip.setDriver(driver);
        Mockito.when(tripRepository.findById(id)).thenReturn(
                trip);

        //Act
        service.addComment(id, comment, user);
    }

    @Test
    public void addComment_Should_saveTripToRepository() {
        // Arrange
        long id = 1;
        Comment comment = new Comment();
        User user = new User(2, "ivan.ivanov");
        Trip trip = new Trip();
        trip.setDriver(user);

        Mockito.when(tripRepository.findById(id)).thenReturn(
                trip);

        //Act
        service.addComment(id, comment, user);

        //
        Mockito.verify(mockRepository, Mockito.times(1)).save(comment);
    }
}
