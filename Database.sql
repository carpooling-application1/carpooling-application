-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.15-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for carpoolingapplication
DROP DATABASE IF EXISTS `carpoolingapplication`;
CREATE DATABASE IF NOT EXISTS `carpoolingapplication` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `carpoolingapplication`;

-- Dumping structure for table carpoolingapplication.comments
DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `userID` bigint(20) DEFAULT NULL,
  `tripID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgv8h0oe18mw92019knhke7bsb` (`userID`),
  KEY `FKpxbmh5puytwtkywe3502j3cbe` (`tripID`),
  CONSTRAINT `FKgv8h0oe18mw92019knhke7bsb` FOREIGN KEY (`userID`) REFERENCES `users` (`id`),
  CONSTRAINT `FKpxbmh5puytwtkywe3502j3cbe` FOREIGN KEY (`tripID`) REFERENCES `trips` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table carpoolingapplication.comments: ~18 rows (approximately)
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
REPLACE INTO `comments` (`id`, `message`, `userID`, `tripID`) VALUES
	(1, 'Really good trip!', 37, 19),
	(2, 'Very happy :)', 38, 19),
	(3, 'It was great!', 33, 21),
	(4, 'I liked it!', 39, 36),
	(5, 'Wow!', 39, 36),
	(8, 'Already there', 39, 36),
	(9, 'It was great!', 33, 19),
	(10, ':)', 33, 19),
	(12, 'Heyy :)', 39, 36),
	(13, '', 39, 36),
	(14, ';)', 39, 36),
	(15, 'Sand, Sun and Sea :)', 39, 36),
	(16, ':)', 39, 40),
	(17, 'Hey', 71, 47),
	(18, ';)', 71, 47),
	(19, ':)', 71, 47),
	(20, ':*', 71, 47),
	(21, '<3', 71, 47);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- Dumping structure for table carpoolingapplication.passenger_details
DROP TABLE IF EXISTS `passenger_details`;
CREATE TABLE IF NOT EXISTS `passenger_details` (
  `passenger` bigint(20) NOT NULL,
  `trip` bigint(20) NOT NULL,
  `ratingGivenByDriver` double NOT NULL DEFAULT 0,
  `ratingGivenByPassenger` double NOT NULL DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`passenger`,`trip`),
  KEY `FKhesyuim2solxd5752smt8qnfy` (`trip`),
  CONSTRAINT `FKhesyuim2solxd5752smt8qnfy` FOREIGN KEY (`trip`) REFERENCES `trips` (`id`),
  CONSTRAINT `FKkkxse42imy071v46qcw9siku5` FOREIGN KEY (`passenger`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table carpoolingapplication.passenger_details: ~16 rows (approximately)
/*!40000 ALTER TABLE `passenger_details` DISABLE KEYS */;
REPLACE INTO `passenger_details` (`passenger`, `trip`, `ratingGivenByDriver`, `ratingGivenByPassenger`, `status`) VALUES
	(33, 20, 0, 5, 0),
	(33, 21, 2, 5, 1),
	(33, 22, 0, 0, 0),
	(33, 23, 0, 3, 0),
	(33, 24, 3, 4, 1),
	(33, 26, 0, 0, 0),
	(33, 31, 0, 0, 0),
	(33, 40, 2, 2, 1),
	(35, 20, 4, 3, 1),
	(36, 22, 2, 3, 1),
	(36, 23, 4, 4, 1),
	(36, 24, 2, 2, 1),
	(37, 19, 2, 1, 1),
	(37, 20, 1, 4, 1),
	(37, 22, 5, 2, 1),
	(38, 19, 3, 1, 1);
/*!40000 ALTER TABLE `passenger_details` ENABLE KEYS */;

-- Dumping structure for table carpoolingapplication.trips
DROP TABLE IF EXISTS `trips`;
CREATE TABLE IF NOT EXISTS `trips` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `availablePlaces` int(11) DEFAULT NULL,
  `carModel` varchar(255) DEFAULT NULL,
  `departureTime` datetime DEFAULT NULL,
  `destination` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `origin` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `driver` bigint(20) DEFAULT NULL,
  `luggage` bit(1) DEFAULT NULL,
  `pets` bit(1) DEFAULT NULL,
  `smoking` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKg3ej8wwynakt0g2q3xyvwxvtm` (`driver`),
  CONSTRAINT `FKg3ej8wwynakt0g2q3xyvwxvtm` FOREIGN KEY (`driver`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- Dumping data for table carpoolingapplication.trips: ~29 rows (approximately)
/*!40000 ALTER TABLE `trips` DISABLE KEYS */;
REPLACE INTO `trips` (`id`, `availablePlaces`, `carModel`, `departureTime`, `destination`, `message`, `origin`, `status`, `driver`, `luggage`, `pets`, `smoking`) VALUES
	(19, 4, 'Audi', '2019-06-06 00:00:00', 'Burgas', 'Let\'s go to the sea!', 'Sofia', 3, 33, b'1', b'1', b'1'),
	(20, 5, 'Opel', '2019-07-07 00:00:00', 'Varna', 'Hey!', 'Plovdiv', 4, 36, b'1', b'1', b'0'),
	(21, 6, 'Mercedes', '2019-10-10 00:00:00', 'Plovdiv', 'Come on!', 'Sofia', 3, 37, b'0', b'0', b'0'),
	(22, 4, 'Audi', '2020-02-05 00:00:00', 'Ruse', 'Come on!', 'Sofia', 3, 38, b'1', b'0', b'0'),
	(23, 5, 'Audi', '2019-10-10 00:00:00', 'Vidin', 'Come on!', 'Sofia', 3, 34, b'1', b'0', b'0'),
	(24, 7, 'Mercedes', '2019-08-07 00:00:00', 'Sofia', 'Don\'t be late!', 'Varna', 3, 35, b'1', b'1', b'1'),
	(25, 5, 'Mercedes', '2019-10-10 00:00:00', 'Plovdiv', 'Don\'t be late!', 'Ruse', 3, 33, b'0', b'1', b'1'),
	(26, 6, 'Mercedes', '2019-10-10 00:00:00', 'Varna', 'Don\'t be late!', 'Ruse', 3, 35, b'0', b'1', b'1'),
	(27, 5, 'Opel', '2020-01-01 00:00:00', 'Plovdiv', 'Hey!', 'Varna', 3, 36, b'0', b'1', b'1'),
	(28, 4, 'Opel', '2020-02-05 00:00:00', 'Varna', 'Don\'t be late!', 'Burgas', 3, 38, b'0', b'0', b'0'),
	(29, 3, 'Audi', '2019-07-17 00:00:00', 'Burgas', 'Let\'s go to the sea!', 'Sofia', 3, 33, b'1', b'1', b'1'),
	(30, 2, 'Audi', '2019-08-08 00:00:00', 'Ruse', 'I am ready!', 'Sofia', 0, 33, b'1', b'1', b'1'),
	(31, 4, 'BMW', '2019-09-09 00:00:00', 'Bansko', 'I am ready! Are you?', 'Plovdiv', 0, 35, b'1', b'1', b'1'),
	(32, 6, 'Audi', '2019-08-08 00:00:00', 'Burgas', 'I am ready! Are you?', 'Plovdiv', 0, 35, b'1', b'1', b'1'),
	(33, 3, 'Mercedes', '2019-08-22 00:00:00', 'Varna', 'I am ready! Are you?', 'Plovdiv', 3, 33, b'1', b'1', b'0'),
	(34, 3, 'BMW', '2019-08-13 00:00:00', 'Ruse', 'Let\'s go!', 'Pleven', 0, 33, b'1', b'0', b'0'),
	(35, 3, 'Mercedes', '2019-08-21 00:00:00', 'Pleven', 'Let\'s go!', 'Plovdiv', 0, 35, b'0', b'1', b'0'),
	(36, 3, 'Mercedes', '2019-08-08 00:00:00', 'Nessebar', 'Let\'s go to the sea!', 'Sofia', 3, 39, b'1', b'1', b'0'),
	(37, 3, 'Mercedes', '2019-08-07 00:00:00', 'Pleven', 'I am ready!', 'Sofia', 0, 39, b'0', b'0', b'0'),
	(38, 2, 'Opel', '2019-08-14 00:00:00', 'Veliko Turnovo', 'I am new here :)', 'Pazardzik', 0, 63, b'0', b'1', b'0'),
	(39, 2, 'Toyota', '2019-08-16 10:00:00', 'Ravda', 'I am new here :)', 'Bankya', 0, 39, b'1', b'1', b'0'),
	(40, 2, 'Toyota', '2019-08-16 10:00:00', 'Nessebar', 'I am new here :)', 'Bankya', 3, 39, b'1', b'1', b'0'),
	(41, 3, 'Toyota', '2019-08-12 00:00:00', 'Varna', 'Let\'s go!', 'Sofia', 0, 39, b'0', b'0', b'0'),
	(42, 2, 'BMW', '2019-08-23 00:00:00', 'Bansko', '', 'Plovdiv', 0, 39, b'0', b'1', b'0'),
	(45, 2, '', '2019-08-23 11:11:00', 'Bansko', 'I am new here :)', 'Pleven', 0, 39, b'0', b'0', b'0'),
	(46, 2, 'Audi', '2019-08-16 11:11:00', 'Burgas', 'Let\'s go!', 'Pleven', 0, 39, b'0', b'1', b'0'),
	(47, 0, 'Mercedes', '2019-08-14 00:00:00', 'Sunny Beach', '', 'Sv. Vlas', 0, 71, b'0', b'0', b'0'),
	(48, 3, 'Audi', '2019-08-16 10:00:00', 'Burgas', 'Let\'s go!', 'Sunny Beach', 0, 71, b'0', b'0', b'0'),
	(49, 3, 'Mercedes', '2019-08-13 16:44:00', 'Sunny Beach', 'Let\'s party! :)', 'Ravda', 0, 33, b'0', b'0', b'0');
/*!40000 ALTER TABLE `trips` ENABLE KEYS */;

-- Dumping structure for table carpoolingapplication.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatarUri` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumping data for table carpoolingapplication.users: ~37 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `avatarUri`, `email`, `firstName`, `lastName`, `password`, `phone`, `username`) VALUES
	(33, '../img/photo-1506919258185-6078bba55d2a.jpg', 'ivan@gmail.com', 'Ivan', 'Ivanov', '$2a$12$7Hqm8vl0dzWjyZJoXqBILum/Jmin7u2RH73bKY6oRm7Rms5QVW5YC', '123456', 'ivan.ivanov'),
	(34, '../img/boy-avatar.png', 'georgi@gmail.com', 'Georgi', 'Georgiev', '$2a$12$HIUQo13nqnpmomU3uh1QRefMsJJ1jFRO97be9wUR5UDJ6NleexcDu', '987654', 'georgi.georgiev'),
	(35, '../img/boy-avatar.png', 'martin@gmail.com', 'Martin', 'Ivanov', '$2a$12$cHWuv6uXcpTZhVnF9.aJCeDdjJnW43YunpBDhclWcCOD.3ufAZIa.', '987654', 'martin.ivanov'),
	(36, '../img/girl-avatar.jpg', 'maria@gmail.com', 'Maria', 'Ivanova', '$2a$12$yEjjVAxJgK3P6Xxhukdtnu9QrryxcUK1lqGIh3.XOH6VMa0FoyGIu', '987654', 'maria.ivanova'),
	(37, '../img/layers-B.jpg', 'krisi@gmail.com', 'Krisi', 'Ivanova', '$2a$12$iiMmrf8F/dQg3Tb743JLbOLOQQSAJCPoxWtyakXzTB3XfoptqjMSK', '987654', 'krisi.ivanova'),
	(38, '../img/fstoppers-natural-light-dani-how-to-retouch-dof-bokeh-sharp-facebook-female-fashion-nyc-model-portrait1.jpg', 'al@gmail.com', 'Alexandra', 'Petrova', '$2a$12$7U5GQUHaHuB.j6PfJrlVou9lw8DyyASvMV7uw/xF9VFMd9pulZuES', '456789', 'al.petrova'),
	(39, '../img/girl-avatar.jpg', 'lk@gmail.com', 'Lyubomira', 'Kostova', '$2a$12$xUGUDm5IcWDkGqyRF9WjsObKxHln64hJnAeHmTW4H/A/08uupiDEq', '888888', 'lyubomira.kostova'),
	(40, '../img/default-avatar.jpg', 'ivana@gmail.com', 'Ivana', 'Petrova', '$2a$12$zylypjsPXxz5hGIcoeHRcuWv070PcB4NPG3zB0fVHhGFgY4pJfs2K', '88888', 'ivana.petrova'),
	(41, '../img/default-avatar.jpg', 'peter@gmail.com', 'Peter', 'Petrov', '$2a$12$d87Yj7ZyqS3fSVWAh6RoQOD0P8o9gHg8T56Dvszjgkvqyg5E/zMey', '88888', 'peter.petrov'),
	(49, '../img/default-avatar.jpg', 'priv@gmail.com', 'Preslava', 'Ivanova', '$2a$12$P5EPNOS69DxUYJv7foERYOxOhKiR.SYek7Dw3xFOWkaQ/QVZZnbrq', '88888', 'preslava.ivanova'),
	(50, '../img/default-avatar.jpg', 'priv@gmail.com', 'Ivana', 'Ivanova', '$2a$12$lYl5v1JXJh0GlMj6P7mSJuBYBLAm8JZhIZYBCg9Lf1W/yp3o4fY7e', '88888', 'ivana.ivanova'),
	(51, '../img/default-avatar.jpg', 'priv@gmail.com', 'Ivana2', 'Ivanova', '$2a$12$Y1x1LBJihJVeZoWtzz7HSu1sTvuj7HR7wZfcclb8XX1OZEnSMFCQe', '88888', 'ivana2.ivanova'),
	(52, '../img/default-avatar.jpg', 'ap@gmail.com', 'Ani', 'Petrova', '$2a$12$Hf3Rh/bHL3sgZKwg1fzQSuQLYGKMrajBEGRnpPx459fC7k0Uhp3C2', '8989', 'ani.petrova'),
	(53, '../img/default-avatar.jpg', 'ap@gmail.com', 'Aniiiii', 'Petrova', '$2a$12$z8VckCHBcVkPrtYoAW6.ou/oOeVy8s0kRV4lXwBchJs3/ecmtiEf2', '8989', 'aniiiii.petrova'),
	(54, '../img/default-avatar.jpg', 'vl.vl@gmail.com', 'Vladimir', 'Vladimirov', '$2a$12$iFrO5Y7Ea2GDZiAERW1hBuas76f33vBwu9eMUyFQNH2Gx.O3BFvfm', '1111', 'vl.vl'),
	(55, '../img/default-avatar.jpg', 'al@gmail.com', 'Alexander', 'Alexandrov', '$2a$12$fnsaIm8/wFgAl9OY.cPKmunxpG3cYaPZXruVVCbOO72NcpVfrIiFS', '1111', 'alexander'),
	(56, '../img/default-avatar.jpg', 'mm@gmail.com', 'Maria', 'Marinova', '$2a$12$fvIOnlx9UTEWDfXCjz41Jenjq93SfnWcJVg/3MUh7or3oc0Znb94C', '1111', 'maria'),
	(57, '../img/default-avatar.jpg', 'ip@gmail.com', 'Iliyana', 'Petrova', '$2a$12$ZFlMO0hiKvkR4aimgvLUWOU8X6Ll9mBRxfCmjJQ7t4lztxFI4T3CW', '1111', 'i.petrova'),
	(58, '../img/default-avatar.jpg', 'iv.petr@gmail.com', 'Ivan', 'Petrov', '$2a$12$KV2U5Z1EXMld2BcrCjcoxO8UfqOcBc6/sDOwoK7/mE54xgzeq.7nK', '1111', 'ivan.petrov'),
	(59, '../img/default-avatar.jpg', 'piv@gmail.com', 'Petya', 'Ivanova', '$2a$12$MZs10/ZRfqfaVgpgnw4PROvFkI1nj5kMa5TfODpRPhVivONbDTJRm', '1111', 'petya.ivanova'),
	(60, '../img/default-avatar.jpg', 'petvlad@gmail.com', 'Peter', 'Vladimirov', '$2a$12$7pquhY3uX5JgOLq1iJbKUuBx4cht7slZP/nJdRNLejj45Y46GVH02', '1111', 'peter.vladimirov'),
	(61, '../img/default-avatar.jpg', 'mpetrova@gmail.com', 'Maria', 'Petrova', '$2a$12$32v7VBul3uPvUNOiJyvByeOPdkuVdb1HhgbPHn0YVnbH8/.4PoNQi', '1111', 'maria.petrova'),
	(62, '../img/default-avatar.jpg', 'ivkonst@gmail.com', 'Ivan', 'Konstantinov', '$2a$12$9Y2WbAHcTQ6FsUjaR7O.Ketd23LKJzXsD3mAXgK.MUGrmDrtFeAku', '1111', 'ivan.konstantinov'),
	(63, '../img/default-avatar.jpg', 'lp@gmail.com', 'Lily', 'Petrova', '$2a$12$2991i/yQpfNtwlFoIOaTmueXbL0vI/.pndLVTvi.1T2aC0/PucA7W', '1111', 'lily.petrova'),
	(64, '../img/girl-avatar.jpg', 'mmrn@gmail.com', 'Maria', 'Marinova', '$2a$12$0kcJn9yM9p/PZgQWrMnn8eiV/ZzYmFhtZXltn7/zwLvREVgfKw6he', '0898', 'maria.marinova'),
	(65, '../img/default-avatar.jpg', 'ii@gmail.com', 'Iliya', 'Iliev', '$2a$12$7ptigBV3JgKaRKOu8FBSPeXXnXnv2vpPlmXY6vkVGEppTMDAQ75kW', '8856', 'ilya.iliev'),
	(66, '../img/default-avatar.jpg', 'ivan@gmail.com', 'Ivan', 'Ivanov', '$2a$12$eertc14hnHjktftoeU7nTe2y1ZaQ.NkieVD4lk2X/nZMsxu.ULsey', '88888', 'ivan.ivanovv'),
	(67, '../img/default-avatar.jpg', 'ivan@gmail.com', 'Ivan', 'Ivanov', '$2a$12$2Uj7qL08hJwOGubbl2yO/.Or0ESbXViEWGsJXbydGSRjngplZSJki', '88888', 'ivan.ivanovvv'),
	(68, '../img/default-avatar.jpg', 'vv@gmail.com', 'Vasil', 'Vasilev', '$2a$12$jmkmANrIpiBNG3VloG4RLOsNNEz2FIqsIM7XY./wA8FEBG4Fp0d8a', '56565', 'vasil.vasilev'),
	(69, '../img/default-avatar.jpg', 'vi@gmamil.com', 'Vanesa', 'Ivanova', '$2a$12$S9yxBmpoc.jz5Lt8B4EfIO/YgZdv3PXIq8k9oclMO0KzicGKGO4j6', '666', 'vanesa.ivanova'),
	(70, '../img/default-avatar.jpg', 'krhr@gmail.com', 'Kristina', 'Hristova', '$2a$12$JT9awslt16FNA.lrHpsZ8ONaHY/a49wisT.eMTJ7OYHZfhajGM3k6', '1111', 'kristina.hristova'),
	(71, '../img/default-avatar.jpg', 'pi@gmail.com', 'Peter', 'Ivanov', '$2a$12$IySb0YnS/Fq95dfsC43ejuKhdGPMx.3Ndqt.zNL/acFT84OpsVNTi', '2222', 'peter.ivanov'),
	(72, '../img/default-avatar.jpg', 'pp@gmail.com', 'Peter', 'Petrov', '$2a$12$l4euW/11tb0OpDfTmEEce.KJWshvWbSWhWXOtbeaQsDC//1xkFB9e', '5656', 'peter.petrovv'),
	(73, '../img/default-avatar.jpg', 'vp@gmail.com', 'Vanya', 'Petrova', '$2a$12$atZEfj0i6MaSqPL5xyHIaOdJ18k0fpeRSVd0sNKjC2FMn14fpzSHy', '5555', 'vanya.petrova'),
	(74, '../img/default-avatar.jpg', 'ds@gmail.com', 'Daniel', 'Stoyanov', '$2a$12$0e9Y4CkVpxZlnHtYnATEP..IWBziQE0ur57b29Ew4H5PYQyLXf9Oq', '66666', 'daniel.stoyanov'),
	(75, '../img/default-avatar.jpg', 'yo@gmail.com', 'Yoan', 'Iliev', '$2a$12$E4BsMomTPwaJFaqOi472TuxavKOUOR.1vzRQq8jquHyG2enuu1KCC', '5555', 'yoan.iliev'),
	(76, '../img/default-avatar.jpg', 'iv@gmail.com', 'Ivan', 'Vladimirov', '$2a$12$iLjZFkNK3Z4cmjwOj1chievoj18FTua4LLMAQwKXSmca.ysAUQsuW', '5555', 'ivan.vladimirov');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table carpoolingapplication.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `User_id` bigint(20) NOT NULL,
  `roles` int(11) DEFAULT NULL,
  KEY `FKjuwnguphj5t4rhh2swd2ag6uv` (`User_id`),
  CONSTRAINT `FKjuwnguphj5t4rhh2swd2ag6uv` FOREIGN KEY (`User_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table carpoolingapplication.user_roles: ~37 rows (approximately)
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
REPLACE INTO `user_roles` (`User_id`, `roles`) VALUES
	(33, 1),
	(34, 1),
	(35, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(41, 1),
	(49, 1),
	(50, 1),
	(51, 1),
	(52, 1),
	(53, 1),
	(54, 1),
	(55, 1),
	(56, 1),
	(57, 1),
	(58, 1),
	(59, 1),
	(60, 1),
	(61, 1),
	(62, 1),
	(63, 1),
	(64, 1),
	(65, 1),
	(66, 1),
	(67, 1),
	(68, 1),
	(69, 1),
	(70, 1),
	(71, 1),
	(72, 1),
	(73, 1),
	(74, 1),
	(75, 1),
	(76, 1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
